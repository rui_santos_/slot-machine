<!doctype html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
  <meta content="utf-8" http-equiv="encoding">-->
  <title>Bitcoin Slot Machine</title>
  <link href="css/style.css" rel="stylesheet">
  <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <!--[if lt IE 9]>
  <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
  <![endif]-->
  <script src="js/jquery.min.js"></script>
  <script src="js/jquery.animate-colors-min.js"></script>
  <script src="bootstrap/js/bootstrap.min.js"></script>
  <script src="js/symbols-table.obfuscated.js"></script>
  <script src="js/mersenne-twister.min.js"></script>
  <script src="js/crc32.min.js"></script>
  <script src="js/sha256.min.js"></script>
  <script src="js/sha1.min.js"></script>
  <script src="js/slot.js"></script>
  <!--<script src="js/slot.min.js"></script>-->
  <script src="js/validate.min.js"></script>
  <script src="js/jquery.form.min.js"></script>
  <!--chat script addded-->
  <!--<script src="https://bitbandit.eu/chat/js/jquery.js" type="text/javascript"></script>-->
  <script src="https://bitbandit.eu/chat/js/yshout.js" type="text/javascript"></script>
  <link rel="stylesheet" href="https://bitbandit.eu/chat/example/css/dark.yshout.css" />
  <script type="text/javascript">
     new YShout({
        yPath: 'https://bitbandit.eu/chat/'
     });
  </script>
</head>