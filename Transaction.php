<?php

/**
 * Description of Transaction
 *
 * @author vadim24816
 *
 *
 */
require_once 'Appconfig.php';

class Transaction {
  private
    $PRECISION,
    $transaction_date,
    $transaction_time,
    $money_amount,
    $transaction_id,
    $deposit,
    $uid;

  public function __construct( $transaction_id = '', $money_amount = 0, $deposit = false, $uid = 0, $PRECISION = 8 ) {
    $this->transaction_id = $transaction_id;
    $this->money_amount = round ( $money_amount, $PRECISION );
    // deposit == 1 -> deposit money
    // deposit == 0 -> withdraw
    // deposit == 2 -> cashback bonus (bonus is calculated from deposited (deposit == 1) money ONLY!,
    // not from money was received by cashback bonus
    $this->deposit = $deposit;
    //save at the creating moment
    $this->transaction_time = ''; // = $transaction_time;
    $this->transaction_date = '';
    $this->uid = $uid;
    $this->save_in_db();
  }

  public function __destruct(){}

  /**
   * real escape transaction obj for DB
   */
  private function real_escape(){
    $db = DBconfig::get_instance();
    $this->transaction_id = $db->mysqli_link->real_escape_string( $this->transaction_id );
    $this->money_amount = $db->mysqli_link->real_escape_string( $this->money_amount );
    $this->deposit = $db->mysqli_link->real_escape_string( $this->deposit );
    $this->transaction_time = $db->mysqli_link->real_escape_string( $this->transaction_time );
    $this->transaction_date = $db->mysqli_link->real_escape_string( $this->transaction_date );
    $this->uid = $db->mysqli_link->real_escape_string( $this->uid );
  }

  public function save_in_db() {
    $db = DBconfig::get_instance();
    $this->real_escape();
    $res = $db->query("INSERT INTO 
      transactions(transaction_id, money_amount, deposit, transaction_date, transaction_time, uid) 
      VALUES('$this->transaction_id', '$this->money_amount', '$this->deposit', NOW(), NOW(), '$this->uid')
    ");
    if (!$res) {
      $error_message = "Save transaction failed. Class ".__CLASS__." Method ".__METHOD__." File ".__FILE__." Line " .__LINE__;
      error_log( $error_message, 0 );
      return FALSE;
    }
    return true;
  }

  public function get_from_db($transaction_id) {
    $db = DBconfig::get_instance();
    $transaction_id =  $db->mysqli_link->real_escape_string($transaction_id);
    $transaction = $db->mysqli_fetch_array('SELECT * FROM transactions WHERE transaction_id = \'' . $transaction_id . '\'');
    //if there is no user with given uid
    if ($transaction == FALSE) {
      $error_message = "Get transaction failed. Class ".__CLASS__." Method ".__METHOD__." File ".__FILE__." Line " .__LINE__;
      error_log( $error_message, 0 );
      return FALSE;
    }
    //the user is found
    else {
      $this->transaction_id = $transaction_id;
      $this->money_amount = $transaction['money_amount'];
      $this->deposit = $transaction['deposit'];
      $this->transaction_date = $transaction['transaction_date'];
      $this->transaction_time = $transaction['transaction_time'];
      $this->uid = $transaction['uid'];
      return TRUE;
    }
  }

  //endNum == 0 --> no limit
  public static function show_transactions($option = 'last', $startNum = 0, $endNum = 20, $from_date = '2012-11-01', $to_date = '2052-11-01', $from_page = 'index', $without_cashback = false) {
    $db = DBconfig::get_instance();
    $option =  $db->mysqli_link->real_escape_string($option);
    $startNum =  $db->mysqli_link->real_escape_string($startNum);
    $endNum =  $db->mysqli_link->real_escape_string($endNum);
    $from_date =  $db->mysqli_link->real_escape_string($from_date);
    $to_date =  $db->mysqli_link->real_escape_string($to_date);
    $from_page =  $db->mysqli_link->real_escape_string($from_page);

    $limit = "LIMIT  $startNum , $endNum";
    if ($endNum == 0)
      $limit = ' ';
    
    $query = 'SELECT * FROM transactions tr1 ';
    //show last 20 by time
    if ($option == 'last') {
      //$query = 'SELECT * FROM transactions ORDER BY `transaction_date` DESC ,  `transaction_time` DESC  '.$limit;
      if ( $without_cashback ){
        $query .= ' WHERE deposit = 0 OR deposit = 1 ';
      }
      //make query without limits
      $query .= 'ORDER BY `transaction_date` DESC ,  `transaction_time` DESC  ';
      //for counting total number of rows
      $total_records_query = $query;
      //limit the query
      $query .= $limit;
    }
    elseif ($option == 'biggestwinners') {
      //$query = 'SELECT * FROM transactions WHERE `deposit` = 0 ORDER BY `money_amount` DESC '.$limit;
      $query .= 'WHERE `deposit` = 0 ORDER BY `money_amount` DESC ';//.$limit;
      $total_records_query = $query;
      $query .= $limit;
    }
    elseif ($option == 'transactions'){
      //$query = "SELECT * FROM transactions WHERE transaction_date BETWEEN '$from_date' AND '$to_date' ORDER BY `transaction_date` DESC ,  `transaction_time` DESC  ".$limit;
      $query .= "WHERE transaction_date BETWEEN '$from_date' AND '$to_date' ";//.$limit;
      if ( $without_cashback ){
        $query .= "AND deposit = 0 OR deposit = 1 ";
      }
      $query .= "ORDER BY `transaction_date` DESC ,  `transaction_time` DESC ";
      //without cashback bonus included
      //$query .= "AND deposit = 0 OR deposit = 1 ";
      $total_records_query = $query;
      $query .= $limit;
    }
    else{
      return false;
    }
    $table_width = 400;
    if ($from_page == 'admin'){
      $table_width = 700;
    }
    //get total number of rows
    $total_records_res = $db->query($total_records_query);
    //$total_records_value = mysqli_num_rows($res);
    $total_records_value = $total_records_res->num_rows;
    //query for transactions (with limits)
    $res = $db->query($query);
    $output = '<div class="transactions"><input id="total_records" type="hidden" value="'.$total_records_value.'"/><table class="table" border="2px" style="width: '.$table_width.'px;">';//start stats table
    $output .=
        '<tr>
        <td>Transaction time</td>
        <td>Money</td>
        <td>Transaction ID</td>';
    if (!empty($_SESSION['admin']) && $from_page == 'admin' &&  $_SESSION['admin'] == 'true'){
      $output .= '<td>UID</td>';
    }
        $output .= '</tr>
      ';
    while ($transactions = $db->mysqli_fetch_array_by_result($res)) {
      if ($transactions['deposit']) {
        $money_column = '<td class="deposit">Deposit: '. round( $transactions['money_amount'], 8) .' </td>';
      }
      else {
        $money_column = '<td class="withdrawn">Withdrawn: '. round( $transactions['money_amount'], 8) .' </td>';
      }
      $output .=
        '<tr>
          <td>' . $transactions['transaction_date'] . ' ' . $transactions['transaction_time'] . '</td>'
        . $money_column .
        '<td><a href="http://blockchain.info/search?search='.$transactions['transaction_id'].'">' . substr($transactions['transaction_id'], 0, 9) . '</a></td>';
      if (!empty($_SESSION['admin']) && $from_page == 'admin' &&  $_SESSION['admin'] == 'true'){
        $output .= '<td>' . $transactions['uid'] . '</td>';
      }
      $output .= '</tr>
      ';
    }
    $output .= '</table></div>';//end
    echo $output;
  }
  public static function show_grouped_by_user($from_date, $to_date){
    $db = DBconfig::get_instance();
    $from_date =  $db->mysqli_link->real_escape_string($from_date);
    $to_date =  $db->mysqli_link->real_escape_string($to_date);
    $query = "
      SELECT t.uid, deposited_table.deposited, withdrawn_table.withdrawn
      FROM 
      transactions t
      LEFT JOIN ( 
        SELECT d1.uid, sum(d1.money_amount) deposited, d1.deposit  
        FROM transactions as d1 
        WHERE 
          d1.deposit = 1 AND 
          d1.transaction_date BETWEEN '$from_date' AND '$to_date'
        GROUP BY d1.uid ) as deposited_table
      ON deposited_table.uid = t.uid
      LEFT JOIN ( 
        SELECT d0.uid, sum(d0.money_amount) withdrawn, d0.deposit  
        FROM transactions as d0 
        WHERE 
          d0.deposit = 0 AND 
          d0.transaction_date BETWEEN '$from_date' AND '$to_date'
          GROUP BY d0.uid ) as withdrawn_table
      ON withdrawn_table.uid = t.uid
      WHERE t.transaction_date BETWEEN '$from_date' AND '$to_date'
      GROUP BY t.uid 
      ORDER BY t.id 
    ";
    $output = "
        <br />
        <table class=\"table\" border=\"1px\">
          <tr>
            <td>Grouped by UID</td>
            <td>Total deposited</td>
            <td>Total withdrawn</td>
            <td>Total deposited - withdrawn</td>
          </tr>
            ";
    $res = $db->query($query);
    while ($transactions_grouped_by_user = $db->mysqli_fetch_array_by_result($res)){
      foreach ($transactions_grouped_by_user as $key => $value) {
        if ($value == NULL){
          $transactions_grouped_by_user[$key] = '0';
        }
      }
      $dif = $transactions_grouped_by_user['deposited'] - $transactions_grouped_by_user['withdrawn'];
      $output .= 
            '<tr>
              <td>'.$transactions_grouped_by_user['uid'].'</td>
              <td>'.$transactions_grouped_by_user['deposited'].'</td>
              <td>'.$transactions_grouped_by_user['withdrawn'].'</td>
              <td>'.$dif.'</td>
            </tr>';
    }
    $output .= '</table>';
    return $output;
  }

  public static function show_cash_in_out_profit_payback_table($from_date, $to_date){
    $db = DBconfig::get_instance();
    $from_date =  $db->mysqli_link->real_escape_string($from_date);
    $to_date =  $db->mysqli_link->real_escape_string($to_date);

    $total_cashed_out = Transaction::get_total_cashed_out_money($from_date, $to_date);
    $total_cashed_in = Transaction::get_total_cashed_in_money($from_date, $to_date);
    if ($total_cashed_in != 0){
      $payback = ($total_cashed_out/$total_cashed_in)*100;
      $payback = round($payback, 2);
    }
    else{
      $total_cashed_in = 0;
      $payback = 100;
    }
    $profit = $total_cashed_in - $total_cashed_out;
    $output_start = "
        <br />
        <table class=\"table\" border=\"1px\" >
          <tr>
            <td>Cash in</td>
            <td>Cash out</td>
            <td>Profit</td>
            <td>Payback %</td>
            ";
    $output_end = "
          </tr>
          <tr>
              <td>$total_cashed_in</td>
              <td>$total_cashed_out</td>
              <td>$profit</td>
              <td>$payback</td>
            </tr>
        </table>
      ";
    echo $output_start.$output_end;
  }

  //just gives a total of all the money cashed out, sums up all the withdraw transactions
  public static function get_total_cashed_out_money($from_date = '2012-11-01', $to_date = '2052-11-01'){
    $db = DBconfig::get_instance();
    $from_date =  $db->mysqli_link->real_escape_string($from_date);
    $to_date =  $db->mysqli_link->real_escape_string($to_date);
    $query = "SELECT SUM(money_amount) FROM transactions WHERE `deposit` = 0  AND `transaction_date` BETWEEN '$from_date' AND '$to_date'";
    $total_cashed_out = $db->mysqli_fetch_array($query);
    if (!$total_cashed_out[0]){
      return 0;
    }
    return round($total_cashed_out[0], 8);
  }
  public static function get_total_cashed_in_money($from_date = '2012-11-01', $to_date = '2052-11-01'){
    $db = DBconfig::get_instance();
    $from_date =  $db->mysqli_link->real_escape_string($from_date);
    $to_date =  $db->mysqli_link->real_escape_string($to_date);
    $query = "SELECT SUM(money_amount) FROM transactions WHERE `deposit` = 1 AND `transaction_date` BETWEEN '$from_date' AND '$to_date'";
    $total_cashed_in = $db->mysqli_fetch_array($query);
    if (!$total_cashed_in[0]){
      return 0;
    }
    return round( $total_cashed_in[0], 8);

  }

  //our profit from user
  public static function get_our_profit_from_user( $uid ){
    $db = DBconfig::get_instance();
    $uid = $db->mysqli_link->real_escape_string( $uid );
    $query = "SELECT t.uid, deposited_table.deposited, withdrawn_table.withdrawn
      FROM
      transactions t
      LEFT JOIN (
        SELECT d1.uid, sum(d1.money_amount) deposited, d1.deposit
        FROM transactions as d1
        WHERE
          d1.deposit = 1 AND
          d1.uid = '$uid'
        GROUP BY d1.uid ) as deposited_table
      ON deposited_table.uid = t.uid
      LEFT JOIN (
        SELECT d0.uid, sum(d0.money_amount) withdrawn, d0.deposit
        FROM transactions as d0
        WHERE
          d0.deposit = 0 AND
          d0.uid = '$uid'
          GROUP BY d0.uid ) as withdrawn_table
      ON withdrawn_table.uid = t.uid
      WHERE t.uid = '$uid'
      GROUP BY t.uid
      ORDER BY t.id ";

    $res = $db->query($query);
    $uid_deposited_withdrawn = $db->mysqli_fetch_array_by_result($res);
      $profit = $uid_deposited_withdrawn['deposited'] - $uid_deposited_withdrawn['withdrawn'];
    return round( $profit, 8);
  }

}