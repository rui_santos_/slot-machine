<?php
require_once 'Appconfig.php';
require_once 'Header.php';
?>
<body>
  <div id="full_list_page">
    <?php
    if ( !empty( $_GET['option'] ) ){
      $option = $_GET['option'];
    }
    else{
      $option = 'last';
    }
    switch ( $option ) {
      case 'last':
        Transaction::show_transactions( 'last', 0, 100 );
        break;
      case 'biggestwinners':
        Transaction::show_transactions( 'biggestwinners', 0, 100 );
        break;

      default:
        Transaction::show_transactions( 'last', 0, 100 );
        break;
    }
    ?>
  </div>
  <div id="prev_and_next">
    <div id="left" style="float: left; display:none;"><a href="#"><<</a></div>
    <div id="right" style="float: right;"><a href="#">>></a></div>
  </div>

  <script type="text/javascript">
    function getParams(){
      var tmp = new Array();
      var tmp2 = new Array();
      GET = new Array();
      var get = location.search;
      if(get != '') {
        tmp = (get.substr(1)).split('&');
        for(var i=0; i < tmp.length; i++) {
          tmp2 = tmp[i].split('=');
          GET[tmp2[0]] = tmp2[1];
        }
      }
      return GET;
    }

    function show(){
      option = 'last';
      GET = getParams();
      option = GET['option'];
      $.post("AjaxRequestsProcessing.php", { slot: "transactions", 'option': option, 'from': (currentPage*paginationPerPage-paginationPerPage), 'to': (currentPage*paginationPerPage)})
        .done(function(transactionsTable) {
          $('div#full_list_page').html(transactionsTable);
          //if (window.console) console.log(transactionsTable);
          pageControl();
        })
        .fail(function(){
          if (window.console) console.log('Client error. Error in ajax admin-->show request, bad response');
        });
    }

    function pageControl(){
      if (currentPage == 1){
        $('div#left').css('display','none');
      }
      else{
        $('div#left').css('display','block');
      }
      if (currentPage == totalPages){
        $('div#right').css('display','none');
      }
      else{
        $('div#right').css('display','block');
      }
    }

    $(document).ready(function(){
      currentPage = 1;
      paginationStart = 0;
      paginationPerPage = 100;
      totalRecords = $( 'div.transactions input#total_records' ).val();
      totalPages = Math.ceil( totalRecords / paginationPerPage );// 1.01 => 2 round to greater int number

      $('div#left').on('click',function(){
        if (currentPage == 1)
          return false;
        currentPage -= 1;
        show();
      });

      $('div#right').on('click',function(){
        if (currentPage == totalPages)
          return false;
        currentPage += 1;
        show()
      });

      pageControl();
    });
  </script>
</body>
</html>