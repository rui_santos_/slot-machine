<?php
require_once 'Appconfig.php';

//fill config info
class ConfigForMyBitcoinClient{
  public function __construct() {
    //for unit test only
    if ( empty( $_SERVER['HTTP_HOST'] ) ){
      $_SERVER['HTTP_HOST'] = 'localhost';
    }
    if ($_SERVER['HTTP_HOST'] == '109.174.40.94' || $_SERVER['HTTP_HOST'] == 'localhost' || $_SERVER['HTTP_HOST'] == '127.0.0.1'){
      $this->scheme = 'https';
      $this->username = 'bitcoinrpc';
      $this->password = 'bBvvbjBP4fSHAnLF38PeHcExtYrCgCRR6j9EL68yEPj';
      $this->address = "localhost";
      $this->port = 8332;
      $this->certificate_path = __DIR__ .'/mysitename.crt';
      $this->debug_level = 0;
    }
    else{
      $this->scheme = 'https';
      $this->username = 'bitcoinrpc';
      $this->password = 'bBvvbjBP4fSHAnLF38PeHcExtYrCgCRR6j9EL68yEPj';
      $this->address = "99.192.139.66";
      $this->port = 8332;
      $this->certificate_path = __DIR__ .'/mysitename.crt';
      $this->debug_level = 0;
    }
  }
  public function __destruct(){}
}

class MyBitcoinClient extends BitcoinClient{
  public static $s = 100;
  public $last_check_time, $last_check_status, $check_time_interval;
  public static function get_instance(){
    //make config
    $config = new ConfigForMyBitcoinClient();
    $mbc = new MyBitcoinClient($config->scheme, $config->username, $config->password, $config->address, $config->port, $config->certificate_path, $config->debug_level);
    $mbc->init();
    return $mbc;
  }
  
  public function init(){
    //keep the last time when connection was checked
    $this->last_check_time = microtime(true);
    $this->last_check_status = true;
    //interval for bitcoin connection checking
    $this->check_time_interval = 15; //15sec
  }
 
  public function check_and_move( $fromaccount = "", $toaccount, $amount, $minconf = 0, $comment = NULL ){
    $fromaccount_money_balance = $this->getbalance($fromaccount, 0);
    //for don't make account balance negative!
    if ($amount > $fromaccount_money_balance){
      return false;
    }
    $this->move($fromaccount, $toaccount, $amount, $minconf, $comment);
  }

  //check bitcoin connection
  public function _check_last_connect(){
    //time now
    $current_check_connect_time = microtime(true);
    //if update connection status period is end
    if ( ($current_check_connect_time - $this->last_check_time) > $this->check_time_interval ){
      $this->last_check_time = $current_check_connect_time;
      $this->last_check_status = ( $this->can_connect() )? true : false;
      return $this->last_check_status;
    }
    //if period is not finished, return last checked status
    else{
      return $this->last_check_status;
    }
  }

  public function check_last_connect(){
    //get current time
    $current_check_connect_time = microtime(true);
    //it make update from DB per every check request
    $db = DBconfig::get_instance();
    $query = 'SELECT `id`, `last_check_status`,  `check_time_interval` ,  `last_check_time` FROM time_intervals WHERE id = "bitcoin"';
    $res = $db->query( $query );
    if ( !$res ){
      $error_message = "Select error. Class ".__CLASS__." Method ".__METHOD__." File ".__FILE__." Line " .__LINE__;
      error_log( $error_message, 0 );
      return '-1';
    }
    $bictoin_row_from_db = $db->mysqli_fetch_array_by_result( $res );
    $this->check_time_interval = $bictoin_row_from_db['check_time_interval'];
    $this->last_check_time = $bictoin_row_from_db['last_check_time'];
    $this->last_check_status = $bictoin_row_from_db['last_check_status'];
    //because of it is received from DB, we have it in 'string' type, so convert it to bool
    $this->last_check_status = ( $this->last_check_status == '1' )? '1' : '0';
    
    // if last check timeout is expired, save current time in $this->last_check_time
    if ( ( $current_check_connect_time - $this->last_check_time ) > $this->check_time_interval ){
      $this->last_check_time = $current_check_connect_time;
      //get current bictoind status
      if ( $this->can_connect() === true ){
        $this->last_check_status = '1';
      }
      else{
        $this->last_check_status = '0';
      }
      //save it in DB
      $query = 'UPDATE time_intervals SET last_check_status = '.$this->last_check_status.', last_check_time = '.$this->last_check_time.' WHERE id="bitcoin"';
      $res = $db->query( $query );
      if ( !$res ){
        $error_message = "Update error. Class ".__CLASS__." Method ".__METHOD__." File ".__FILE__." Line " .__LINE__;
        error_log( $error_message, 0 );
        return '-1';
      }
      //and return NEW value of last check
      return $this->last_check_status;
    }
    // if NOT expired, get values from DB
    else{
      //and return value of last check from DB
      return $this->last_check_status;
    }
  }

  public static function is_valid_bitcoin_address( $address ){
    //check bitcoin address
    $address_pattern = '/^[a-zA-Z0-9]{34}$/';
    if(preg_match($address_pattern, $address) === 1) {
      return true;
    }
    else{
      return false;
    }
  }

  //todo:
  public function is_account_exist($account){
    return true;
  }
  //todo:
  //can return WRONG address because of bitcoin
  public function get_sender_bitcoin_address($account){
    
  }
  public function __destruct() {
  }
}
