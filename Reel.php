<?php

/**
 * Description of Reel
 *
 * @author vadim24816
 */

require_once 'Appconfig.php';

class Reel {
  public $reel_line = array(),$name;
  public function __construct($name) {
    $this->name = $name;
  }
  public function __destruct(){}
  //return new symbol choosed randomly
  //args - (string $client_seed, string $server_seed)
  public function get_new_randomly_choosed_symbol($hashed_client_seed, $server_seed){
    //real client_seed is crc32(sha1(client_seed))
    $client_seed = crc32($hashed_client_seed);
    $server_seed = crc32($server_seed);
    $result_seed = ($client_seed + $server_seed);
    //get instance
    $randomizer = Randomizer::get_instance();
    //get instance from Session
    //separated mersenne twiester lib
    $rand_num = abs($randomizer->mersenne_twister_int32($result_seed) % 64);
    //default php mt rand generator
    $sym = $this->reel_line[$rand_num];
    $debug_data = array(
        'rand_num' => $rand_num, 
        'result_seed' => $result_seed, 
        'client_seed' => $client_seed, 
        'server_seed' => $server_seed, 
        );
    return $sym;
  }

  public function filling_by_given_symbol_specifin_number_of_cells($symbol, $number){
    for($i = 0; $i < $number; $i++){
      array_push($this->reel_line, $symbol);
    }
  }
}