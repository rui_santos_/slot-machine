<?php
/**
 * Created by JetBrains PhpStorm.
 * User: vadim28416
 * Date: 02.05.13
 * Time: 20:05
 * To change this template use File | Settings | File Templates.
 */

require_once _DIR__.'/../MyBitcoinClientTest.php';
require_once 'PHPUnit/Autoload.php';

class MyBitcoinClientTest extends PHPUnit_Framework_TestCase {
  function test_is_valid_bitcoin_address_valid(){
    $mbc = MyBitcoinClient::get_instance();
    $this->assertTrue( MyBitcoinClient::is_valid_bitcoin_address('123eKpBDgvo2sGo1yr8QCYtb1e1WvyMpue') );
    $this->assertTrue( MyBitcoinClient::is_valid_bitcoin_address('188ifGmQ1mNa4vYeo64fgk7JuHTptLEfaY') );
    $this->assertTrue( MyBitcoinClient::is_valid_bitcoin_address('12G1SXgVt7imf8Led5Y1RSLweJpy8wTJFG') );
    $this->assertTrue( MyBitcoinClient::is_valid_bitcoin_address('188ifGmQ1mNa4vYeo64fgk7JuHTptLEfaY') );

    $this->assertFalse( MyBitcoinClient::is_valid_bitcoin_address('') );
    $this->assertFalse( MyBitcoinClient::is_valid_bitcoin_address('1') );
    $this->assertFalse( MyBitcoinClient::is_valid_bitcoin_address('-123eKpBDgvo2sGo1yr8QCYtb1e1WvyMpu') );
    $this->assertFalse( MyBitcoinClient::is_valid_bitcoin_address('123eKpBDgvo2sGo1yr8QCYtb1e1WvyMpue1') );
    $this->assertFalse( MyBitcoinClient::is_valid_bitcoin_address('jjjj') );
  }

}
