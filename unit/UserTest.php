<?php
/**
 * Created by JetBrains PhpStorm.
 * User: vadim28416
 * Date: 02.05.13
 * Time: 18:37
 * To change this template use File | Settings | File Templates.
 */

class UserTest extends PHPUnit_Framework_TestCase {
  /*
  function test_set_bitcoin_receive_address(){
    $user = User::get_instance();
    $user->set_bitcoin_receive_address();
    $mbc = MyBitcoinClient::get_instance();
    $this->assertTrue( MyBitcoinClient::is_valid_bitcoin_address( $user->bitcoin_receive_address ) );
  }*/
  function test_match_to_regexp(){
    $phpsessid_pattern = '/^[a-zA-Z0-9,-]{10,32}$/';
    $phpsessid_right = 'a67r26g7d8cteb07162uagvb51,-';
    $phpsessid_wrong = 'aa67r26g7d8cteb07162uagvb51$@$()&*%';
    $this->assertTrue( User::match_to_regexp( $phpsessid_pattern, $phpsessid_right ) );
    $this->assertFalse( User::match_to_regexp( $phpsessid_pattern, $phpsessid_wrong ) );
  }
  function test_are_username_and_password_valid(){
    $this->assertTrue( User::are_username_and_password_valid('asfd', 'asdfasfd', 'asdfasfd') );
    $this->assertTrue( User::are_username_and_password_valid('asfd123', 'asdfasfd', 'asdfasfd') );
    $this->assertTrue( User::are_username_and_password_valid('asfd', 'asdfasfd123456789', 'asdfasfd123456789') );
    $this->assertTrue( User::are_username_and_password_valid('asfd', 'asdfasfd', 'asdfasfd') );

    $this->assertFalse( User::are_username_and_password_valid('as', 'asdfasfd', 'asdfasfd') );
    $this->assertFalse( User::are_username_and_password_valid('asasfdsf', 'as', 'as') );
    $this->assertFalse( User::are_username_and_password_valid('asdf1234asdf1234asdf1234asdf1234asdf1234asdf1234asdf1234asdf1234a', 'asdfasfd', 'asdfasfd') );
    $this->assertFalse( User::are_username_and_password_valid('asdf1234asdf1234asdf1234asdf1234asdf1234asdf1234asdf1234asdf1234', 'asdf1234asdf1234asdf1234asdf1234asdf1234asdf1234asdf1234asdf1234a', 'asdf1234asdf1234asdf1234asdf1234asdf1234asdf1234asdf1234asdf1234a') );
    $this->assertFalse( User::are_username_and_password_valid('asasdf', 'aaasdfasfd', 'asdfasfd') );
    $this->assertFalse( User::are_username_and_password_valid('asasdf', 'asdfasfd', 'asdfasfdbb') );
    $this->assertFalse( User::are_username_and_password_valid('asas@#$%^&*()df', 'asdfasfd', 'asdfasfdbb') );
    $this->assertFalse( User::are_username_and_password_valid('asasdf', 'asdfas-fd', 'asdfas-fd') );
  }
  function test_is_username_already_exists(){
    $uid_by_name = '1e52e0a1a74c04cf560bc536efe342cac69507e5';
    $exist_name = 'userA';
    $non_exist_name = 'user123asdf';
    $this->assertEquals( User::is_username_already_exists( $exist_name ), $uid_by_name );
    $this->assertFalse( User::is_username_already_exists( $non_exist_name ) );
  }
  function test_is_valid_uid(){
    $this->assertTrue( User::is_valid_uid( '1e52e0a1a74c04cf560bc536efe342cac69507e5') );
    $this->assertFalse( User::is_valid_uid( '1e52e0a1a7404cf560bc536efe342cac69507e5') );
    $this->assertFalse( User::is_valid_uid( '1e52e0a1a74c04cf560bc536efe342cac69507e50') );
    $this->assertFalse( User::is_valid_uid( '1e52e0a1a74c04cf560bc536efe342cac69507e-') );
  }
  function test_real_escape(){
    $user = User::get_instance();
    $user->uid = "1e52e0a1a74'c04cf560'bc536efe342cac69507e5";
    $user->username = "e0a1a74'c04cf5";
    $user->md5_password = "e0a1a74'c04cf5";
    $user->phpsessid = "1e52e0a1a74'c04cf560'bc536efe342cac69507e5";
    $user->user_wallet = "1e52e0a1a74'c04cf560'bc536efe342cac69507e5";
    $user->user_wallet_for_registered_user = "1e52e0a1a74'c04cf560'bc536efe342cac69507e5";
    $user->bitcoin_receive_address = "1e52e0a1a74'c04cf560'bc536efe342cac69507e5";
    $user->money_balance = "0.015'5";
    $user->affiliateusername = "userA'B'C";
    $user->remote_user_address = "127.000.00'0.001";
    $user->last_check_time = "0.015'5";
    $user->bitcoin_money_balance = "0.015'5";
    $user->real_escape();

    $this->assertFalse( "1e52e0a1a74'c04cf560'bc536efe342cac69507e5" === $user->uid);
    $this->assertTrue( "1e52e0a1a74\'c04cf560\'bc536efe342cac69507e5" === $user->uid);

    $this->assertTrue( "e0a1a74\'c04cf5" === $user->username);
    $this->assertTrue( "e0a1a74\'c04cf5" === $user->md5_password);
    $this->assertTrue( "1e52e0a1a74\'c04cf560\'bc536efe342cac69507e5" === $user->phpsessid );
    $this->assertTrue( "1e52e0a1a74\'c04cf560\'bc536efe342cac69507e5" === $user->user_wallet );
    $this->assertTrue( "1e52e0a1a74\'c04cf560\'bc536efe342cac69507e5" === $user->user_wallet_for_registered_user );
    $this->assertTrue( "1e52e0a1a74\'c04cf560\'bc536efe342cac69507e5" === $user->bitcoin_receive_address );
    $this->assertTrue( "0.015\'5" === $user->money_balance );
    $this->assertTrue( "userA\'B\'C" === $user->affiliateusername );
    $this->assertTrue( "userA\'B\'C" === $user->affiliateusername );
    $this->assertTrue( "127.000.00\'0.001" === $user->remote_user_address );
    $this->assertTrue( "0.015\'5" === $user->last_check_time );
    $this->assertTrue( "0.015\'5" === $user->bitcoin_money_balance );
  }


}
