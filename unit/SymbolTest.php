<?php
/**
 * Created by JetBrains PhpStorm.
 * User: vadim28416
 * Date: 02.05.13
 * Time: 15:52
 * To change this template use File | Settings | File Templates.
 */

require_once __DIR__.'/../Symbol.php';
require_once 'PHPUnit/Autoload.php';

class SymbolTest extends PHPUnit_Framework_TestCase {
  /*
   * Test method must start with "test"
   * like "test_..."
   * */
  function test_is_symbol_return_true_for_symbol_and_false_for_non_symbol() {
    $this->assertTrue(Symbol::is_symbol(Symbol::$pyramid));
    $this->assertTrue(Symbol::is_symbol(Symbol::$bitcoin));
    $this->assertTrue(Symbol::is_symbol(Symbol::$anonymous));
    $this->assertTrue(Symbol::is_symbol(Symbol::$onion));
    $this->assertTrue(Symbol::is_symbol(Symbol::$anarchy));
    $this->assertTrue(Symbol::is_symbol(Symbol::$peace));
    $this->assertTrue(Symbol::is_symbol(Symbol::$blank));
    $this->assertTrue(Symbol::is_symbol(Symbol::$any));
    $this->assertTrue(Symbol::is_symbol('pyramid'));
    $this->assertFalse(Symbol::is_symbol('dfsfdsfd'));
  }
}