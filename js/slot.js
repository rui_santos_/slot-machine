function Slot(){
      var slot = this;
      slot.currentBet = 0;
      slot.lastPayline = '';
      slot.currentPayline = '';
      slot.emptyPayline = {bet_from_client: "0", multiplier: 0, sym1: "blank", sym2: "blank", sym3: "blank"};
      slot.autoplay = false;
      slot.currentUserBalance = 0;
      slot.currentClientSeed = '123456';
      slot.lastBet = 0;
      slot.symbolsPerReel = 64;
      slot.arrayOfSymbolsId = new Array();
      slot.onceFilledLine = false;
      slot.onceStarted = false;
      slot.internetConnection = true;
      slot.rotationTime = {
        //rotation time for every reel in ms
        reel1: 2000,
        reel2: 3000,
        reel3: 5000
      };
      //the lowest value will be 0.12345678 btc
      slot.roundTo = 1e8;
      //max spin time is the max time of every reel rotates
      slot.maxSpinTime = Math.max(slot.rotationTime.reel1,slot.rotationTime.reel2,slot.rotationTime.reel3);
      slot.totalSymbolsNumber = 7;
      slot.state = 'stop';
      //make slot state 'stop'
      slot.getStateStop = function(){
        slot.state = 'stop';
      }
      //make slot state 'started'
      slot.getStateStarted = function(){
        slot.state = 'started';
      }
      slot.timeouts = {
        bitcoinConnection: 15000, //15s and out by timeout
        checkSlotOptionsInterval: 5*60*1000, //5min = 5*60*1000
        //checkForNewIncommingPaymentInterval: 5000, //5s || 60s
        checkForNewIncommingPaymentInterval: 3000, //3s - before first payment was received
        afterFirstPaymentWasReceived: 61000, //61s - after first payment was received
        //bitcoinCheckConnectionInterval: 10000, //10s
        bitcoinCheckConnectionInterval: 23000, //23s
        updateInterestingFactsInterval: 10000
      }
      //check matching server seed and its hash
      slot.isUnhashedAndHashedSeedsMatch = function(jsonServerSeed, hashedServerSeed){
        var cryptObj = new CryptoJS.SHA1(jsonServerSeed);
        var hashedJsonServerSeed = cryptObj.toString();
        if (window.console){
          console.log('jsonServerSeed = '+jsonServerSeed);
          console.log('hashedJsonServerSeed = '+hashedJsonServerSeed);
          console.log('hashedServerSeed = '+hashedServerSeed);
        }
        if (hashedJsonServerSeed == hashedServerSeed){
          return true;
        }
        else{
          return false;
        }
      };
      //verify (int)client seed, (array) serverSeeds and symbols are got from last spin 
      slot.calcSymbolsFromClientSeedAndServerSeeds = function(clientSeed, serverSeeds){
        //get CryptoJS obj from client seed
        var CryptoJSobj = CryptoJS.SHA1(clientSeed.toString());
        //get hash Of Client Seed in HEX format
        var hashOfClientSeed = CryptoJSobj.toString(CryptoJS.enc.Hex);
     
        //numbers of symbols on every reel
        var symNums = new Array(3);
        var twisters = new Array(3)
        for (var i=0; i<3; i++){
          //get twisted sequence from given seeds
          twisters[i] = new MersenneTwister(crc32_x64(hashOfClientSeed)+crc32_x64(serverSeeds[i]));
          //get number from 0 to 63
          symNums[i] = twisters[i].genrand_int32() % 64;
        }
        
        var wt = new weightTable();
        var symbols = new Array();
        for (var i=0; i<3; i++){
          symbols[i] = wt.getSymNameOnReelByNumber(i, symNums[i]);
        }
        var symbolsJson = JSON.stringify(symbols);
        return symbolsJson;
      };
      slot.bitcoinConnection = {
        //slot.bitcoinConnection.newCheck used for checking connection with bitcoin
        //use for cashout
        newCheck: false,
        //use for checking new payments
        lastCheck: true,
        checkForBitcoinConnection : function(){
          //no connection newCheck 
          slot.bitcoinConnection.newCheck = false;
          return $.ajax({
            type: "POST",
            url: "AjaxRequestsProcessing.php",
            dataType: "text",
            data: { slot: "bitcoin_connect" },
            //set timeout for request
            timeout: slot.timeouts.bitcoinConnection
          })
            .done(function(is_bitcoin_connected){
              if (window.console) { console.log(is_bitcoin_connected); }
              is_bitcoin_connected = $.parseJSON(is_bitcoin_connected);
              slot.bitcoinConnection.newCheck = ( is_bitcoin_connected ) ? true : false;
              //if response was got then there is internet connection
              slot.internetConnection = true;
              if (window.console) { console.log('bitcoin connection = '+slot.bitcoinConnection.newCheck)};
            })
            .fail(function(){
              slot.bitcoinConnection.newCheck = false;
              //no response == no connection
              slot.internetConnection = false;
              if (window.console) { console.log('Exit by timeout  '+slot.timeouts.bitcoinConnection+'ms. Bitcoin connection = '+slot.bitcoinConnection.newCheck)};
            })
            .always (function(){
              if (window.console) { console.log('checkForBitcoinConnection exit. bitcoin connection = '+slot.bitcoinConnection.newCheck)};
              //keep last checking
              slot.bitcoinConnection.lastCheck = slot.bitcoinConnection.newCheck;
            })
        }
        
      };
      slot.musicOn = true;
      slot.audio = {
        spin5sec: null,
        win: null,
        loose: null,
        spinbutton: null,
        cashregister: null,
        //in case if buttons pressed oftenly
        buttons: {
          element: null,
          play: function(){
            try{
              //stop emulation
              slot.audio.buttons.element.pause();
              slot.audio.buttons.element.currentTime = 0;
              slot.audio.buttons.element.play();
            }
            catch(e){
              if (window.console){ console.warn(e+" Audio doesn't supported")}
            }
          }
        }
      }
      //by default set the last bet before every new spin
      slot.setLastBetByDefault = function(){
        if (slot.currentBet == 0){
          slot.setBetTo(slot.getLastBet());
        }
      }
      //just warm-up if have no deposit 
      slot.warmup = {
        isLastWarned : false,
        isWarmup : true,
        //10 times of spin without deposite
        count : 10,
        dec : function(){
          if (this.count > 1){
            this.count -= 1;
          }
          else{  //if (this.count < 0)
            this.count -= 1;
            this.isWarmup = false;
            //slot.statusDisplay.show(this.lastWarn(), 15);
          }
        },
        couldWin : function(){
          return "YOU could have WON "+slot.currentPayline.multiplier+" times your bet.\n Please feed me some bitcoins! :)";
        },
        couldntWin : function(){
          return "You lost nothing. Let's try for real now? :)";
        },
        lastWarn : function(){
          if ( !slot.warmup.isLastWarned && !slot.warmup.isWarmup ){
            //show last warning only 1 time after 10 free spins
            slot.warmup.isLastWarned = true;
            return "No more free spins buddy, time to play for real :)";
          }
        }
      }
      //sounds
      slot.initAudio = function(){
        slot.audio.spin5sec = document.getElementById('spin5sec');
        slot.audio.win = document.getElementById('win');
        slot.audio.loose = document.getElementById('loose');
        slot.audio.spinbutton = document.getElementById('spinbutton');
        slot.audio.buttons.element = document.getElementById('buttons');
        slot.audio.cashregister = document.getElementById('cashregister');
      }
      //generate new or get entered by user
      slot.getClientSeed = function(){
        var seed_entered_by_client = $('div#verify input#client_seed').val();
        var client_seed;
        if (typeof(seed_entered_by_client) != 'string' 
          || seed_entered_by_client.length == 0 
          || slot.currentClientSeed == seed_entered_by_client){// 
          client_seed = Math.round(Math.random()*2000000);//0--2 000 000
        }
        else{
          client_seed = seed_entered_by_client;
        }
        $('div#verify input#client_seed').val(client_seed)
        return client_seed;
      }
      //checking using multiplier in payline that was returned by server
      function isWin(){
        if (slot.currentPayline.multiplier > 0){
          return true;
        }
        else{
          return false;
        }
      }
      slot.winChecker = function(){
        //in case of warmup - just first 10 times
        if (slot.warmup.isWarmup){
          //if win
          if (isWin()){
            slot.statusDisplay.show(slot.warmup.couldWin(), 12);
            try{
              slot.audio.win.play();
            }
            catch(e){
              if (window.console){ console.warn(e+" Audio doesn't supported")}
            }
          }
          //if lose
          else if (!isWin()){
            slot.statusDisplay.show(slot.warmup.couldntWin());
            try{
              slot.audio.loose.play();
            }
            catch(e){
              if (window.console){ console.warn(e+" Audio doesn't supported")}
            }
          }
          //else if(slot.warmup.)
        }
        //normal mode
        else {
          if (isWin()){
            //won_money = round(multiplier * bet)
            var won_money = Math.round((slot.currentPayline.multiplier*slot.currentPayline.bet_from_client) * slot.roundTo ) / slot.roundTo ;
            slot.statusDisplay.show('You WON '+ won_money +' (Bet '+slot.currentPayline.bet_from_client+'x'+slot.currentPayline.multiplier+')', 17);
            try{
              slot.audio.win.play();
            }
            catch(e){
              if (window.console){ console.warn(e+" Audio doesn't supported")}
            }
          }
          if(!isWin() && slot.currentPayline.bet_from_client){//slot.currentPayline.bet_from_client != 'undefined'
            slot.statusDisplay.show('You lost '+ slot.currentPayline.bet_from_client +'. Better luck on the next spin!', 17);
            try{
              slot.audio.loose.play();
            }
            catch(e){
              if (window.console){ console.warn(e+" Audio doesn't supported")}
            }
          }
        }
      }
      //display beneath the slot
      slot.statusDisplay = {
        messages : {
          connectionError : "There seems to be connectivity problem, please check your internet connection",
          connectionRecover : "The connection is back on, you can play!",
          noBitcoinConnection : "Can't check incoming payment, no bitcoin connection.",
          bitcoinConnectionRecover : "Bitcoin connection is recovered.",
          playingOff : "Playing is off",
          payingOutOff: "Paying out is off",
          playingAndPayingOutOff: "Playing and paying out are off",
          registeredAndNotLoggedCashoutAndPlay: "You should be logged in to cash out and playing"
        },
        //get message
        get : function(){
          return $('div#slots-status-display').text();
        },
        //show message
        show : function(str){
          $('div#slots-status-display').css('opacity',1);
          //default font size
          var fontSize = 17;
          //if other font size given
          if (arguments.length == 2){
            //if given as string
            if (typeof arguments[1] == 'string'){
              fontSize = parseInt(arguments[1]);
            }
            if (typeof arguments[1] == 'number'){
              fontSize = arguments[1];
            }
          }
          else{
            fontSize = 17;
          }
          $('div#slots-status-display').css('font-size', fontSize);
          $('div#slots-status-display').text(str);
        },
        clear : function(delay){//set delay in ms
          delay = (arguments.length == 1) ? parseInt(delay) : 500; //default delay = 500
          $('div#slots-status-display').stop().animate({opacity: 0}, delay);
          setTimeout("$('div#slots-status-display').text('');", delay);
        }
      };
      //default options. updated from server
      slot.options = {
          'playing': 'on',
          'paying_out': 'on',
          'maxbet': 0
        };
      slot.checkSlotOptions = function(){
        $.post("AjaxRequestsProcessing.php", { slot: "options", power: 'check_options'})
        .done(function(options) {
          slot.internetConnection = true;
          if (window.console) console.log(options);
          options = $.parseJSON(options);
          slot.options.playing = options.playing;
          slot.options.paying_out = options.paying_out;
          slot.options.maxbet = options.maxbet;
          if ((slot.options.playing == 'off') && (slot.options.paying_out == 'off')){
            //slot.statusDisplay.show('Playing and paying out are off');
            slot.statusDisplay.show(slot.statusDisplay.messages.playingAndPayingOutOff);
          }
          else if (slot.options.playing == 'off'){
            //slot.statusDisplay.show('Playing is off');
            slot.statusDisplay.show(slot.statusDisplay.messages.playingOff);
          }
          else if (slot.options.paying_out == 'off'){
            //slot.statusDisplay.show('Paying out is off');
            slot.statusDisplay.show(slot.statusDisplay.messages.payingOutOff);
          }
          else if (
            slot.statusDisplay.get() == 'Paying out is off' || 
            slot.statusDisplay.get() == 'Playing is off' || 
            slot.statusDisplay.get() == 'Playing and paying out are off'
          ){
            slot.statusDisplay.clear();
          }
          
          //if last message was an fail
          if (slot.statusDisplay.get() == slot.statusDisplay.messages.connectionError){
            slot.statusDisplay.show(slot.statusDisplay.messages.connectionRecover);
          }
        })
        .fail(function(){
          if (window.console) console.error('Client error. Error in ajax admin-->options power request, bad response');
          slot.statusDisplay.show(slot.statusDisplay.messages.connectionError, 15);
          slot.internetConnection = false;
          //slot.statusDisplay.show('Connection fail.');
        });
      };
      slot.updateInterestingFacts = function(){
        $.post("AjaxRequestsProcessing.php", { slot: "getInterestingFacts"})
        .done(function(interestingFacts) {
          interestingFacts = $.parseJSON(interestingFacts);
          $('td#total_cached_out').text('Cashed out money: '+ interestingFacts.cashed_out_money +' BTC');
          $('td#total_spin_number').text('Games played: '+ interestingFacts.games_played);
          if (window.console) console.log('updateInterestingFacts');
        })
        .fail(function(){
          if (window.console) console.error('Client error. Error in ajax updateInterestingFacts request, bad response');
          //slot.statusDisplay.show('Connection fail.');
        });
      }
      slot.checkForNewIncommingPayment = function(){
        //no connection, no money
        if ( !slot.bitcoinConnection.lastCheck && 
              slot.statusDisplay.get() != slot.statusDisplay.messages.connectionError ){
          slot.statusDisplay.show(slot.statusDisplay.messages.noBitcoinConnection, 15);
          if (window.console) console.warn('No bitcoin connection');
          return false;
        }
        //if connection was recovered
        if ( slot.statusDisplay.get() == slot.statusDisplay.messages.noBitcoinConnection &&
             slot.statusDisplay.get() != slot.statusDisplay.messages.connectionError){
          slot.statusDisplay.show(slot.statusDisplay.messages.bitcoinConnectionRecover);
          slot.statusDisplay.clear(2000);
        }
        
        $.post("AjaxRequestsProcessing.php", { slot: "checkForIncommingPayment"})
        .done(function(payment) {
          slot.internetConnection = true;
          payment = $.parseJSON(payment);
          //sync only if balances different
          payment.amount = parseFloat(payment.amount);
          //payment.tab
          if (payment.amount > 0){
            slot.timeouts.checkForNewIncommingPaymentInterval = slot.timeouts.afterFirstPaymentWasReceived;//61sec after first payment was received
            $('div#slots-body').trigger('newIncommingPayment', ['false', 'Param 2']);
            //update balance
            var newBalance = slot.currentUserBalance + payment.amount;
            slot.setCurrentUserBalance(slot.currentUserBalance + payment.amount);
            slot.audio.cashregister.play();
            //for sync user obj on client with user_wallet from User obj on server
            //slot.syncWithServer();
            slot.syncWithServer().done(function(){
              //update balance
              slot.updateBalanceAndBet();
              //update bitcoin user_wallet address
              slot.updateUserWalletBitcoinAddress();
              slot.updateLinksAndLabel();
              //updateRegisterLinkAndForm
            });
            if (window.console){
              console.log('balance.amount = '+payment.amount);
              console.log('newBalance = '+newBalance);
              console.log('slot.currentUserBalance = '+slot.currentUserBalance);
              console.log('Balance updated');
              console.log(payment);
            }
          }
          //if last message was a fail
          if (slot.statusDisplay.get() == slot.statusDisplay.messages.connectionError){
            slot.statusDisplay.show(slot.statusDisplay.messages.connectionRecover);
          }
        })
        .fail(function(){
          if (window.console) console.error('Client error. Error in ajax checkForIncommingPayment request, bad response');
          slot.statusDisplay.show(slot.statusDisplay.messages.connectionError, 15);
          slot.internetConnection = false;
        })
        .always(function(){
          
        });
      }
      
      slot.getHashedServerSeeds = function(){
        var getHashedServerSeeds;
        $.post("AjaxRequestsProcessing.php", { slot: "getHashedServerSeeds"})
        .done(function(getHashedServerSeeds){
          getHashedServerSeeds = $.parseJSON(getHashedServerSeeds);
          $('div#verify input#new_hashed_server_seed').val(getHashedServerSeeds);
          //if (window.console) console.log('getHashedServerSeeds = '+getHashedServerSeeds);
        })
        .fail(function(){
          if (window.console) console.error('Client error. Error in ajax result server seed request, bad response');
        });
      }
      
      slot.sendToServerThatSpinPressed = function(){
        //fill payline by blank symbols
        slot.fillPayLine(slot.emptyPayline);
        //get random client seed or entered by user
        var clientSeed = slot.getClientSeed();
        //keep it
        slot.currentClientSeed = clientSeed;
        $.post("AjaxRequestsProcessing.php", { slot: "spinPressed", currentBet: slot.currentBet, clientSeed: clientSeed })
        .done(function(arrayReturnedByServerSpin) {
          slot.internetConnection = true;
          arrayReturnedByServerSpin = $.parseJSON(arrayReturnedByServerSpin);
          
          if (arrayReturnedByServerSpin == -1){
            slot.options.playing = 'off';
            slot.statusDisplay.show('Slot machine is off');
            return false;
          }
          //if (paylineReturnedByServerSpin == '[Bet <= 0 or Bet not number.]'){
          if (arrayReturnedByServerSpin == -2){
            slot.statusDisplay.show('Bad bet');
            return false;
          }
          if (arrayReturnedByServerSpin == -900){
              slot.statusDisplay.show('Wrong bet');
              return false;
          }

          slot.currentPayline = arrayReturnedByServerSpin['new_payline'];
          slot.fillPayLine(slot.currentPayline);
          slot.rememberLastShowedSymbols();
          
          //here will be other hashed server seeds already!
          slot.getHashedServerSeeds();
          //when result (new payline, user win/lose) was received, client sync with server
          slot.syncWithServer();
          
          //if client has made bet then he stars to play with real money
          if (slot.currentPayline.bet_from_client > 0){
            slot.warmup.isWarmup = false;
          }
          var symbolsJson = slot.calcSymbolsFromClientSeedAndServerSeeds(clientSeed, arrayReturnedByServerSpin['server_seeds']);
          var lastHashedServerSeed = $('div#verify input#new_hashed_server_seed').val();
          //fill last hashed server seed field
          $('div#verify input#last_hashed_server_seed').val(lastHashedServerSeed);
          
          //show symbols after spin is finished(!)
          setTimeout(function(){
            $('div#verify input#symbols').val(symbolsJson);
            $('div#verify input#server_seed').val(JSON.stringify(arrayReturnedByServerSpin['server_seeds']));
          }, slot.maxSpinTime-500);
          
          //if last message was an fail
          if (slot.statusDisplay.get() == slot.statusDisplay.messages.connectionError){
            slot.statusDisplay.show(slot.statusDisplay.messages.connectionRecover);
          }
          
          //do event "responseSpinGetting" when response was really got
          $('div#slots-body').trigger('responseSpinGetting', ['true', 'Param2']);
          return true;
        })
        .fail(function(){
          if (window.console) console.error('Client error. Error in ajax spin request, bad response');
          slot.statusDisplay.show(slot.statusDisplay.messages.connectionError, 15);
          slot.internetConnection = false;
          $('div#slots-body').trigger('responseSpinGetting', ['false', 'Param2']);
        });
      }
      slot.symbols = {
        'pyramid': 0,
        'bitcoin': 1,
        'anonymous': 2,
        'onion': 3,
        'anarchy': 4,
        'peace': 5,
        'blank': 6
      }
      //set user balance
      slot.setCurrentUserBalance = function(money_balance){
        slot.currentUserBalance = money_balance;
      }
      //check for multi tabs
      slot.checkMultiTabs = function(openedWindow){
        //if this window is first opened, create new window
        if ( openedWindow.isset == 'no' ){
          window.name = openedWindow.window_name;
          if (window.console) console.log(' create new window ');
        }
        //other window
        else{
          //set new unique window
          if ( !window.name ){
            if (window.console) console.log(' set new unique window ');
            //make request for new window name
            $.post("AjaxRequestsProcessing.php", {
              slot: "sync",
              action: "set_new_window_name",
              dataType: 'json'
            })
              .done( function( result ) {
                result = $.parseJSON(result);
                window.name = result.opened_window.window_name;
                if (window.console){
                  console.log(' new unique window is setted window.name = '+window.name);
                  console.log('result');
                  console.log(result.opened_window);
                }
              })
          }
          //if mismatch - block this window
          else if (window.name != openedWindow.window_name){
            //global flag of blocked window
            window._slotWindowIsBlocked = true;
            //switch off autoplay for blocked window
            if ( window._slotWindowIsBlocked ){
              slot.autoplay = false;
            }
            if (window.console) console.log('mismatch. window.name != openedWindow.window_name ');
            if ($('#fading_double_window').size() == 0){
              $("body").append('<div id="fading_double_window" ><p id="activate_new_wind" style="text-align:center;"><span style="color:#fff;">Make this window active?</span></p></div>');
              body_height = document.height || document.documentElement.scrollHeight;
              screen_height = screen.height;

              $('#fading_double_window').css({
                'position':'absolute',
                'top':'0px',
                'left':'0px',
                'width':'100%',
                'height':'100%',
                'background-color':'#000',
                'z-index':'9999999',
                'cursor':'pointer'
              }).click(function(){
                  $.post("AjaxRequestsProcessing.php", {
                    slot: "sync",
                    action: "set_new_window_name",
                    dataType: 'json'
                  })
                    .done( function( result ) {
                      result = $.parseJSON(result);
                      window.name = result.opened_window.window_name;
                      if (window.console){
                        console.log(' set_new_window_name window.name = '+window.name);
                        console.log('result');
                        console.log(result.opened_window);
                      }
                    })
                  $(this).remove();
                });

              $("#fading_double_window").css("height", body_height).fadeTo(0, 0.8);

              $('#activate_new_wind').css(
                {
                  'position': 'fixed',
                  'top':((screen_height/2)-50),
                  'width': '100%'
                }).fadeTo(0,1);
            }
          }
          // window is active
          else{
            if ( $('#fading_double_window').size()>0 ){
              $( '#fading_double_window' ).remove();
            }
            if (window.console) console.log(' window is active ');
          }
        }
      }
      //full sync with server
      slot.syncWithServer = function(){
        user = null;
        window.user = user;
        return $.post("AjaxRequestsProcessing.php", { slot: "sync", action: "check_window_name" })
          .done(function(result) {
            var parsedResult = $.parseJSON(result);
            user = parsedResult.user;
            var openedWindow = parsedResult.opened_window;
            slot.setCurrentUserBalance( user.money_balance - slot.currentBet );
            if ( user.money_balance > slot.currentBet ){
              //set bet but WITHOUT update instantly
              slot.setBetTo(slot.currentBet, false);
            }
            else{
              slot.setBetTo(user.money_balance, false);
            }
            slot.checkMultiTabs(openedWindow);
          })
          .fail(function(){
            if (window.console) console.error('Error in syncWithServer');
          });
      }
      //show last symbols
      slot.lastShowedSymbols = {
        //fill it by blank symbols
        reel1: {top: 6, center: 6, bottom: 6},
        reel2: {top: 6, center: 6, bottom: 6},
        reel3: {top: 6, center: 6, bottom: 6}
      }
      
      slot.getValidSymbolByName = function(symbol){
        if (typeof(symbol) == 'number' && symbol >= 0 && symbol <= 6){
          return symbol;
        }
        if ( typeof(slot.symbols[symbol]) == 'undefined'){
          return false;
        }
        else{
          return slot.symbols[symbol];
        }
      }
      //fill payline
      slot.fillPayLine = function(payline){
//        $('div#slots-reel1 > div.slots-line > div.payline').attr('class', 'slots-symbol'+slot.getValidSymbolByName(slot.currentPayline.sym1)+' payline');
//        $('div#slots-reel2 > div.slots-line > div.payline').attr('class', 'slots-symbol'+slot.getValidSymbolByName(slot.currentPayline.sym2)+' payline');
//        $('div#slots-reel3 > div.slots-line > div.payline').attr('class', 'slots-symbol'+slot.getValidSymbolByName(slot.currentPayline.sym3)+' payline');

          $('div#slots-reel1 > div.slots-line > div.payline').attr('class', 'slots-symbol'+slot.getValidSymbolByName(payline.sym1)+' payline');
          $('div#slots-reel2 > div.slots-line > div.payline').attr('class', 'slots-symbol'+slot.getValidSymbolByName(payline.sym2)+' payline');
          $('div#slots-reel3 > div.slots-line > div.payline').attr('class', 'slots-symbol'+slot.getValidSymbolByName(payline.sym3)+' payline');
      }
      
      //fill line (top/center/bottom) in slot
      slot.fillLine = function(symbol){
        $('div.slots-line').append('<div class="slots-symbol'+ symbol +'"></div>');
      }
      //There is no spoon
      slot.restoreLastShowedSymbols = function(){
        $('div#slots-reel1 > div.slots-line > div.oldpayline').prev().attr('class', 'slots-symbol'+slot.lastShowedSymbols.reel1.top);
        $('div#slots-reel1 > div.slots-line > div.oldpayline').attr('class', 'slots-symbol'+slot.lastShowedSymbols.reel1.center+' oldpayline');
        $('div#slots-reel1 > div.slots-line > div.oldpayline').next().attr('class', 'slots-symbol'+slot.lastShowedSymbols.reel1.bottom);
        
        $('div#slots-reel2 > div.slots-line > div.oldpayline').prev().attr('class', 'slots-symbol'+slot.lastShowedSymbols.reel2.top);
        $('div#slots-reel2 > div.slots-line > div.oldpayline').attr('class', 'slots-symbol'+slot.lastShowedSymbols.reel2.center+' oldpayline');
        $('div#slots-reel2 > div.slots-line > div.oldpayline').next().attr('class', 'slots-symbol'+slot.lastShowedSymbols.reel2.bottom);
        
        $('div#slots-reel3 > div.slots-line > div.oldpayline').prev().attr('class', 'slots-symbol'+slot.lastShowedSymbols.reel3.top);
        $('div#slots-reel3 > div.slots-line > div.oldpayline').attr('class', 'slots-symbol'+slot.lastShowedSymbols.reel3.center+' oldpayline');
        $('div#slots-reel3 > div.slots-line > div.oldpayline').next().attr('class', 'slots-symbol'+slot.lastShowedSymbols.reel3.bottom);
      }
      
      //remember who you are
      slot.rememberLastShowedSymbols = function(){
        slot.lastShowedSymbols.reel1.top = slot.arrayOfSymbolsId[0][0];
        slot.lastShowedSymbols.reel1.center = slot.getValidSymbolByName(slot.currentPayline.sym1);
        slot.lastShowedSymbols.reel1.bottom = slot.arrayOfSymbolsId[0][2];
        
        slot.lastShowedSymbols.reel2.top = slot.arrayOfSymbolsId[1][0];
        slot.lastShowedSymbols.reel2.center = slot.getValidSymbolByName(slot.currentPayline.sym2);
        slot.lastShowedSymbols.reel2.bottom = slot.arrayOfSymbolsId[1][2];
        
        slot.lastShowedSymbols.reel3.top = slot.arrayOfSymbolsId[2][0];
        slot.lastShowedSymbols.reel3.center = slot.getValidSymbolByName(slot.currentPayline.sym3);
        slot.lastShowedSymbols.reel3.bottom = slot.arrayOfSymbolsId[2][2];
      }
      //fills lines of symbols for all reels 
      slot.linesFilling = function(){
        var reelNumber = 0;
        $('div.slots-line').each(function(){
          slot.arrayOfSymbolsId[reelNumber] = new Array();
          for (var i = 0; i < slot.symbolsPerReel; i++) {
            var id = Math.round(Math.random()*(slot.totalSymbolsNumber-1));
            //reelLineSymbols keep wrong id for payline!
            //because payline gets new id after request to server
            slot.arrayOfSymbolsId[reelNumber][i] = id;
            if (!slot.onceFilledLine){
              $(this).append('<div class="slots-symbol'+ id +'"></div>');
            }
            else{
              $(this).children('div :nth-child('+(i+1)+')').attr('class','slots-symbol'+ id);
            }
          }
          $(this).css({marginTop: -(slot.symbolsPerReel-3)*125 + 'px'});
          reelNumber++;
        });
        
        //add classes oldpayline and payline
        $('div.slots-line :nth-child(2)').addClass('payline');
        $('div.slots-line :nth-child(63)').addClass('oldpayline');
        if (!slot.onceFilledLine){
          slot.onceFilledLine = true;
          slot.rememberLastShowedSymbols();
        }
      }
      //Main function. Make 1 spin
      slot.spin = function(){
        //check if user registered but not logged in
        if ( window.user.is_registered && !window.user.is_logged ){
          slot.statusDisplay.show( slot.statusDisplay.messages.registeredAndNotLoggedCashoutAndPlay, 15 );
          //don't let him make spin
          return false;
        }

        if (slot.internetConnection == false){
          return false;
        }
        
        if (slot.options.playing == 'off'){
          //slot.statusDisplay.show('Playing is off');
          slot.statusDisplay.show(slot.statusDisplay.messages.playingOff);
          if (window.console) console.log('Playing off');
          return false;
        }
        //already started
        if (slot.state == 'started'){
          if (window.console) console.log('[Slot started already. Wait while it have stoped! ]');
          return false;
        }
        
        //if real money was deposited, but no bet had made
        if (slot.currentUserBalance > 0 && slot.currentBet <= 0){
          slot.statusDisplay.show("Please place a bet");
          //then warmup is over
          slot.warmup.isWarmup = false;
          return false;
        }
        //no money deposited and no warmup times rest
        if (slot.currentBet <= 0 && slot.currentUserBalance <= 0 && slot.warmup.isWarmup == false){
          slot.statusDisplay.show(slot.warmup.lastWarn(), 15);
          if (window.console) console.log('[Current bet = 0. Not worth the trouble]');
          return false;
        }
        //if user already played for real, so it isn't need to warn him
        if (slot.currentUserBalance > 0){
          slot.warmup.isLastWarned = true;
        }
        
        //------------------------------------------------------------------------------------------------------
        //spin really started from here-------------------------------------------------------------------------
        //------------------------------------------------------------------------------------------------------
        
        if ( slot.autoplay ){
          //status display shows message before spin longer  if autoplay, then clear it
          setTimeout('slot.statusDisplay.clear()', Math.round(slot.maxSpinTime/1.5));
        }
        else{
          setTimeout('slot.statusDisplay.clear()', Math.round(slot.maxSpinTime/4));
        }
        
        //restore last showed symbols if there is last show exists
        slot.linesFilling();
        if (slot.onceStarted){
            slot.restoreLastShowedSymbols();
        }
        
        //play only if spin was ran
        try{
          slot.audio.spin5sec.play();
        }
        catch(e){
          if (window.console){ console.log(e+" Audio doesn't supported")}
        }
        
        //save last payline
        slot.lastPayline = slot.currentPayline;
        slot.sendToServerThatSpinPressed();
        //slot started
        
        slot.getStateStarted();
//        setTimeout('slot.getStateStop()', slot.maxSpinTime);
        //bet was 
        slot.lastBet = slot.currentBet;
        slot.currentBet = 0;
        slot.updateBalanceAndBet();
        slot.animateSlot();

        slot.onceStarted = true;
        
        setTimeout('slot.spinFinished()', slot.maxSpinTime+100);
        return true;
      }
      slot.spinFinished = function(){
        slot.getStateStop();
        //make after spin
        slot.setLastBetByDefault();
        slot.winChecker();
        // -1 warmup spin
        if (slot.warmup.isWarmup){
          slot.warmup.dec();
        }
        //update balance
        slot.updateBalanceAndBet();
        
        //event spinFinished
        $('div#slots-body').trigger('spinFinished', ['false', 'Param2']);
      }
      //
      slot.afterSpinResponseGetting = function(){

      }
      slot.animateSlot = function(){
        $('div.slots-line').each(function(){
          $(this).css({marginTop: -(slot.symbolsPerReel-3)*125 + 'px'});
        });
        $('div#slots-reel1 > div.slots-line').animate({marginTop: 0}, slot.rotationTime.reel1);
        $('div#slots-reel2 > div.slots-line').animate({marginTop: 0}, slot.rotationTime.reel2);
        $('div#slots-reel3 > div.slots-line').animate({marginTop: 0}, slot.rotationTime.reel3);
      }

      slot.incBetTo = function(val){
        slot.setBetTo(slot.currentBet + val);
      }
      slot.decBetTo = function(val){
        slot.setBetTo(slot.currentBet - val);
      }

      //set bet to val
      slot.setBetTo = function(val, update){
        if ( typeof(update) == 'undefined' ){
          update = true;
        }
        val = parseFloat(val);
        val = Number(val);
        val = Math.round( val * slot.roundTo ) / slot.roundTo;
        //val should be >= 0
        if (val < 0){
          //return 0;
          val = 0;
        }
        //val should be < currentUserBalance
        if ( 
            (slot.currentUserBalance + slot.currentBet - val < 0) || 
            (val > slot.options.maxbet)){
          if (window.console) console.log("[Not enough money or bet too big. Max bet had been made.]");
          val = slot.getMaxBet();
          //return 0;
        }
        //prev bet get back to balance
        slot.currentUserBalance = slot.currentBet + slot.currentUserBalance;
        //make bet
        slot.currentBet = val;
        //balance minus bet
        slot.currentUserBalance -= slot.currentBet;
        //round all
        slot.currentUserBalance = Math.round(slot.currentUserBalance * slot.roundTo) / slot.roundTo;
        slot.currentBet = Math.round(slot.currentBet * slot.roundTo) / slot.roundTo;
        //don't update instantly in slot.syncWithServer
        if ( update ){
          slot.updateBalanceAndBet();
        }
      }
      //retfresh values on page
      slot.updateBalanceAndBet = function(){
        slot.currentUserBalance = Math.round(slot.currentUserBalance * slot.roundTo) / slot.roundTo;
        slot.currentBet = Math.round(slot.currentBet * slot.roundTo) / slot.roundTo;
        $('div#slots-balance').text(slot.currentUserBalance);
        $('div#slots-bet').text(slot.currentBet);
      }

      //update address to which user should send money
      slot.updateBitcoinAddress = function(){
        $('#slots-address').text(user.bitcoin_receive_address);
      }

      //update address to which user should receive money
      slot.updateUserWalletBitcoinAddress = function(){
        $('#own_address').val(user.user_wallet);
      }

      //update links: register, login, logout; form:login,register; label own_address_label
      slot.updateLinksAndLabel = function (){
        $('a#affiliate_link').text( 'https://bitbandit.eu/?r='+user.username );
        $('#logout_link').hide();
        //not regged and not logged
        if ( !user.is_registered && !user.is_logged ){
          $('#register_link').show();
          $('#login_link').show();
          $('#logout_link').hide();
          $('div#div_affiliate_link').hide();
          //update lable above cashout user's bitcoin address
          $('span#own_address_label').text('You can log in and paste your own receive bitcoin address here:');
        }
        //regged and not logged
        else if ( user.is_registered && !user.is_logged ){
          $('#register_link').show();
          $('#login_link').show();
          $('#logout_link').hide();
          $('div#div_affiliate_link').hide();
          //update lable above cashout user's bitcoin address
          $('span#own_address_label').text('You can log in and paste your own receive bitcoin address here:');
        }
        //regged and logged
        else if ( user.is_registered && user.is_logged ){
          $('#register_link').hide();
          $('#login_link').hide();
          $('#logout_link').show();
          $('div#div_affiliate_link').show();
          //update lable above cashout user's bitcoin address
          $('span#own_address_label').text('You can paste your own receive bitcoin address here:');
        }
        //now! for all users who has no money on slot balance, even if user have money on bet
        //check slot.currentUserBalance for cashout, not user.money_balance
        if ( slot.currentUserBalance == 0){
          //replace the label text
          $('span#own_address_label').text('You have nothing to cashout');
        }
      }

      //return max bet and fill current bet field
      slot.getMaxBet = function(){
        if ( slot.currentUserBalance + slot.currentBet <= 0 ){
          return 0;
        }
        //sum all money
        var maxMoney = slot.currentUserBalance + slot.currentBet;
        //cut off up to 3 decimal
        //e.g.: Math.floor(0.0029654*MIN_BET_INVERT)*MIN_BET; // => 0.002
        maxMoney = Math.floor( maxMoney * MIN_BET_INVERT ) * MIN_BET;
        //return all user money or maxbet possible
        return (maxMoney <= slot.options.maxbet) ? maxMoney : slot.options.maxbet;
      }
      slot.makeMaxBet = function(){
        var maxBet = slot.getMaxBet();
        slot.setBetTo(maxBet);
      }
      slot.getLastBet = function(){
        if (slot.lastBet == 0){
          if (window.console) console.log('[There is no last bet]');
          return 0;
        }
        return slot.lastBet;
      }

      slot.isValidBitcoinAddress = function (address){
        //check bitcoin address
        var address_pattern = '^[a-zA-Z0-9]{34}$';
        var matchedAddress = address.match(address_pattern);
        console.log(matchedAddress);
        if ( matchedAddress == null ){
          return false;
        }
        if( matchedAddress.length == 1 ) {
          return true;
        }
        else{
          return false;
        }
      }


      slot.makeCashoutRequest = function(){
        //get entered address
        var enteredUserWallet = $("input#own_address").val();
        //get address to which we will send money ( == user_wallet by default)
        var userReceivingAddress = user.user_wallet;
        //if entered is valid
        if ( slot.isValidBitcoinAddress( enteredUserWallet ) ){
          //send to entered
          userReceivingAddress = enteredUserWallet;
          $("div#wrong_bitcoin_address").css("display", "none");
        }
        else{
          //show error
          $("div#wrong_bitcoin_address").css("display", "block");
          $("div#default_confirmed_cashout_message").css("display", "none");
          //blocking sending
          $('button#make_cashout_request').removeAttr('disabled');
          return false;
        }
        $.post("AjaxRequestsProcessing.php", { slot: "cashOut", user_entered_wallet: userReceivingAddress, amount: slot.currentUserBalance})//cash out only user balance, without bet
          .done(function(cashOut) {
            cashOut = $.parseJSON(cashOut);
            //hide waiting bar
            $("div#default_confirmed_cashout_message").css("display", "none");
            //if not enough confirmations (2 at least)
            if (cashOut.amount == '-101'){
              $("div#not_confirmed_cashout_message").css("display", "block");
              $("div#confirmed_cashout_message").css("display", "none");
            }
            //enough confirmations
            else if (cashOut.amount > '0'){
              var txid = '<a href="http://blockchain.info/search?search='+cashOut.txid+'">'+ cashOut.txid +'</a>';
              $("div#confirmed_cashout_message span#txid").html(txid);
              $("div#not_confirmed_cashout_message").css("display", "none");
              $("div#confirmed_cashout_message").css("display", "block");
              //sound of money balance changed
              slot.audio.cashregister.play();
              slot.syncWithServer()
                .success(function() {
                  slot.updateBalanceAndBet();
                });
            }
            //spin is running => no cashout
            else if (slot.state == "started"){
              $("div#spin_is_running_cashout_message").css("display", "block");
            }
            //zero on balance
            else if (cashOut.amount == '0'){
              $("div#zero_cashout_message").css("display", "block");
            }
            //slot machine is off
            else if (cashOut.amount == '-200'){
              $("div#paying_out_off_message").css("display", "block");
            }
            //money were won > slot wallet amount
            else if (cashOut.amount == '-500'){
              $("div#big_money_message").css("display", "block");
            }
            //wrong bitcoin address
            else if (cashOut.amount == '-666'){
              $("div#wrong_bitcoin_address").css("display", "block");
            }
            if (window.console) console.log('cashOut: ');
            if (window.console) console.log(cashOut);
            //sync money after withdrawn
            slot.syncWithServer();
          })
          .fail(function(){
            if (window.console) console.error('Bad request in isBitcoinConnected function');
          })
          .always(function(){
            $('button#make_cashout_request').removeAttr('disabled');
          })
//        $('button#make_cashout_request').removeAttr('disabled');
      }
      
    }
//    console.log('some msg');
//    console.info('information');
//    console.warn('some warning');
//    console.error('some error');
//    console.assert(false, 'YOU FAIL');
    