<?php
//for include/exclude minified js
define('DEBUG_MODE', true);
try{
  require_once 'Appconfig.php';
  $user = $_SESSION['User'];
}
catch (Exception $e){
  dump_it($e->getTraceAsString());
}
require_once 'Header.php';
?>
<body>
  <div id="slots">
    <div id="slots-reel1" class="slots-reel">
      <div class="slots-line"></div>
    </div>
    <div id="slots-reel2" class="slots-reel">
      <div class="slots-line"></div>
    </div>
    <div id="slots-reel3" class="slots-reel">
      <div class="slots-line"></div>
    </div>
    <div id="slots-status">
      <div id="slots-status-display"></div>
    </div>
    <div id="register" class="register_and_affiliate" data-toggle="modal" data-target="#register_modal">
      <h5>
        <a href="#register_login_modal_window" id="register_link" style="display: none;" data-toggle="modal"
           onclick="
           $('#signin').slideUp('fast');
           $('#signup').slideDown('fast');
           $('.error_box').fadeOut(0);
           $('.success_box').fadeOut(0);
           //$('button.create_my_account').attr('disabled', 'disabled');
           //$('button.create_my_account').removeAttr('disabled');
           "
           >Register</a>
      </h5>
      <h5>
        <a href="#register_login_modal_window" id="login_link" style="display: none;" data-toggle="modal"
           onclick="
           $('#signin').slideDown('fast');
           $('#signup').slideUp('fast');
           $('.error_box').fadeOut(0);
           $('.success_box').fadeOut(0);
           //$('button.login').attr('disabled', 'disabled');
           //$('button.login').removeAttr('disabled');
           "
           >Login</a>
      </h5>
      <h5>
        <a href="#register_login_modal_window" id="logout_link" style="display: none;" >Logout</a>
      </h5>
    </div>
    <div class="to_affiliates" id="div_affiliate_link" style="color: #BBB; display:none">
      Your affiliates:
      <a href="affiliates.php">here</a>
      <!--          <a href="--><?php //echo $affiliate_link_url = "https://" . $_SERVER['HTTP_HOST'] . "/?affiliateusername=" . $user->username; ?><!--" id="affiliate_link">--><?php //echo $affiliate_link_url; ?><!--</a>-->
    </div>
    <!--just for activating tooltips-->
    <script type="text/javascript">
    $(function () {
        $("[rel='popover']").popover();
    });
    </script>
    <!--  Verify  -->
    <div id="verify">
      <div class="verify_field">
        <label class="field_name">Client seed</label>
        <span class="field_question badge badge-info" rel="popover" data-title="Client seed" data-placement="top" data-content="
          This is the first of the two values which have direct effect on generating of random symbols.
          You can put here any string or number. If you don't enter anything, string will be randomly filled with your browser.
          So you have direct impact on generating of random symbols and
          before the spin slot-machine doesn't know what value will be used and it can't adjust its seed for your loosing.
        ">?</span>
        <br />
        <!--(Your lucky number)-->
        <input id="client_seed" name="client_seed" type="text" value="" />
      </div>
      <div class="verify_field">
        <label class="field_name">New Hash(server seed)</label>
        <span class="field_question badge badge-info" rel="popover" data-title="New hashed server seed" data-placement="top" data-content="
          This is the second of the two values. It is provided by slot-machine.
          Because it was hashed, you can't to know what the values actually will be used for generating of symbols before a spin.
          After the spin slot-machine put here new hashed seed and this field copies to the field below.
        ">?</span>
        <br />
        <input readonly id="new_hashed_server_seed" name="hashed_server_seed" type="text" value="" />
      </div>
      <div class="verify_field">
        <label class="field_name">Last Hash(server seed)</label>
        <span class="field_question badge badge-info" rel="popover" data-title="Last hashed server seed" data-placement="top" data-content="
        After the spin was done the copy of the field above will be placed here.
        ">?</span>
        <br />
        <input readonly id="last_hashed_server_seed" name="hashed_server_seed" type="text" value="" />
      </div>
      <div class="verify_field">
        <label class="field_name">Server seed</label>
        <span class="field_question badge badge-info" rel="popover" data-title="Server seed" data-placement="top" data-content="
          Here you can see what really values was used for the seeding of random symbols generator.
          You can sure that exactly these values was provided by slot-machine before the spin
          (for generating of random symbols) if you hash this string using SHA1 function.
          Or just click &quot;Verify&quot; button and it will be done automatically.
          If everything ok, fields will be highlighted blue color.
          So you can be sure that slot-machine doesn&apos;t adjust the values for to its advantage.
        ">?</span>
        <br />
        <input readonly id="server_seed" name="server_seed" type="text" value="" />
      </div>
      <div class="verify_field">
        <label class="field_name">Symbols</label>
         <span class="field_question badge badge-info" rel="popover" data-title="Symbols" data-placement="top" data-content="
        The last field contains the symbols which are generated using exactly these client and server seeds.
        When you press &quot;Verify&quot; the field is highlighted with green
        if client and server seeds correspond to symbols are outputed and red otherwise.
        So if you change client seed and press &quot;Verify&quot; the color of field &quot;Symbols&quot; may be changed to red in case of mismatching
        the symbols for given seeds and ones was generated
        ">?</span>
        <br />
        <input readonly id="symbols" name="symbols" type="text" value="" />
      </div>
      <button id="verify_button" class="btn" aria-hidden="true">Verify</button>
    </div>
    <!--  /Verify  -->
      <!--  Chat  -->
<div id="yshout"></div>
    <!--  Slots body  -->
    <div id="slots-body">
      <img id="slots-logo" src="images/bsm-logo.png" alt="SatoshiSlots.com">
      <img id="slots-paytable" src="images/bsm-paytable.png" alt="Paytable">
      <div id="slots-address" class="slots-display"> <?php echo $user->bitcoin_receive_address; ?> </div>
      <div id="slots-balance" class="slots-display">0</div>
      <div id="slots-bet" class="slots-display">0</div>
      <button id="slots-minus001" class="slots-button slots-minus"></button>
      <button id="slots-minus01" class="slots-button slots-minus"></button>
      <button id="slots-minus1" class="slots-button slots-minus"></button>
      <button id="slots-plus001" class="slots-button slots-plus"></button>
      <button id="slots-plus01" class="slots-button slots-plus"></button>
      <button id="slots-plus1" class="slots-button slots-plus"></button>
      <button id="slots-spin" class="slots-button"></button>
      <button id="slots-lastbet" class="slots-button slots-bottombutton"></button>
      <button id="slots-maxbet" class="slots-button slots-bottombutton"></button>
      <button id="slots-autoplay" class="slots-button slots-bottombutton"></button>
      <button id="slots-cashout" class="slots-button slots-bottombutton" data-toggle="modal" data-target="#cashout_modal"></button>
    </div>
<!--  Statistic tables  -->
  <div id="statistic">
    <?php
      echo '<span class="table_header">Last 20 transactions</span> <br />';
      Transaction::show_transactions($option = 'last', NULL, NULL, NULL, NULL, NULL, true );
      echo '<a href="fullList.php?option=last">Full list</a><br /><br />';
      echo '<span class="table_header">20 biggest winners</span> <br />';
      Transaction::show_transactions($option = 'biggestwinners');
      echo '<a href="fullList.php?option=biggestwinners">Full list</a><br /><br />';
      echo '<span class="table_header">Interesting facts</span> <br />';
      show_interesting_facts();
    ?>
  </div>
  <!--  /Statistic tables  -->
    <!--  /Slots body  -->
  </div>
  <!-- Modal cashout dialog -->
  <div id="cashout_modal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="Cashout" aria-hidden="true">
    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
      <h3 id="Cashout">Cashout</h3>
    </div>
    <div class="modal-body">
      <!--  (Default) -->
      <div id="default_confirmed_cashout_message" >
        <h5>Please, wait...</h5>
        <div class="progress progress-striped active">
          <div class="bar" style="width: 100%;"></div>
        </div>
      </div>
      <!--No bitcoin connection-->
      <div id="no_bitcoin_connection_message" style="display: none" class="alert alert-error">
        <span class="label label-error">No bitcoin connection</span>
        <p>
          No bitcoin connection. Please try later.
        </p>
      </div>
      <!--  display when money which user sent was confirmed  -->
      <div id="confirmed_cashout_message" style="display: none" class="alert alert-success">
        <span class="label label-success">Success</span>
        <p>
          Your coins were sent successfully, here is the transaction ID: <br />
          <span id="txid">THE_TRANSACTION_ID_FROM_BLOCKCHAIN.INFO HERE</span>
        </p>
      </div>
      <!--  display when money which user sent was NOT confirmed  -->
      <div id="not_confirmed_cashout_message" style="display: none" class="alert alert-info">
        <span class="label label-info">Info</span>
        <p>
          There aren't enough confirmations of your deposit just yet. <br />
          Please wait 10 mins and try again. Thank you!
        </p>
      </div>
      <!--  Zero balance  -->
      <div id="zero_cashout_message" style="display: none" class="alert alert-info">
        <p>
          <span class="label label-info">Info</span>
          Your balance is 0
        </p>
      </div>
      <div id="paying_out_off_message" style="display: none" class="alert alert-info">
        <p>
          <span class="label label-info">Info</span>
          Paying out is off now.
        </p>
      </div>
      <!-- Big win -->
      <div id="big_money_message" style="display: none" class="alert">
        <p>
          <span class="label label-warning">Big win!</span>
          There are not enough funds in the hot wallet to pay that. Please contact support to get payment from the secure cold wallet
        </p>
      </div>
      <!-- Trying to cashout when game is running -->
      <div id="spin_is_running_cashout_message" style="display: none" class="alert alert-info">
        <p>
          <span class="label label-info">Info</span>
          You can cashout only when the game is not running
        </p>
      </div>
      <!-- Can't send money, wrong user bitcoin address -->
      <div id="wrong_bitcoin_address" style="display: none" class="alert alert-block">
        <p>
          <span class="label label-important">Wrong bitcoin address</span>
          Can't send money, wrong user bitcoin address. Please, contact support.
        </p>
      </div>
     <!-- Trying to cashout when game is running -->
      <div id="user_own_bitcoin_address" class="own_address" style="display: block" class="alert alert-info">
        <p>
          <span class="label label-info">Info</span>
          <span id="own_address_label" >(Your bitcoin address)</span><br/>
          <input id="own_address" name="user_own_bitcoin_address" type="text" value=""/>
        </p>
      </div>
    </div>
    <div class="modal-footer">
      <button id="make_cashout_request" class="btn btn-info" data-dismiss="" aria-hidden="true">Ok</button>
      <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
    </div>
  </div>
  <!-- /Modal cashout dialog -->

  <!--  Modal Register   -->
  <div id="register_login_modal_window" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="registerModal" aria-hidden="true">
    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    </div>
    <div class="modal-body">
        <div class="well">
          <div id="error_messages" class="error_box">
          </div>
          <div id="success_messages" class="success_box">
            All of the fields were successfully validated!
          </div>
          <form name="register_form" id="signup" class="form-horizontal" method="post" action="AjaxRequestsProcessing.php">
            <input type="hidden" name="slot" value="signup" >
            <legend>Sign Up</legend>
            <div class="control-group">
              <label class="control-label">Username</label>
              <div class="controls">
                <div class="input-prepend">
                  <span class="add-on"><i class="icon-user"></i></span>
                  <input type="text" class="input-xlarge" id="username" name="username" placeholder="Username">
                </div>
              </div>
            </div>
            <div class="control-group">
              <label class="control-label">Password</label>
              <div class="controls">
                <div class="input-prepend">
                  <span class="add-on"><i class="icon-lock"></i></span>
                  <input type="Password" id="password" class="input-xlarge" name="password" placeholder="Password">
                </div>
              </div>
            </div>
            <div class="control-group">
              <label class="control-label">Confirm Password</label>
              <div class="controls">
                <div class="input-prepend">
                  <span class="add-on"><i class="icon-lock"></i></span>
                  <input type="Password" id="confirm_password" class="input-xlarge" name="confirm_password" placeholder="Re-enter Password">
                </div>
              </div>
            </div>
            <div class="control-group">
              <label class="control-label"></label>
              <div class="controls">
                <button type="submit" class="btn btn-success create_my_account" >Create My Account</button>
              </div>
            </div>
          </form>
<!--          /Register form-->
<!--          Login form-->
          <form name="login_form" id="signin" class="form-horizontal" method="post" action="AjaxRequestsProcessing.php">
            <input type="hidden" name="slot" value="signin" >
            <!--            AjaxRequestsProcessing.php-->
            <legend>Log in</legend>
            <div class="control-group">
              <label class="control-label">Username</label>
              <div class="controls">
                <div class="input-prepend">
                  <span class="add-on"><i class="icon-user"></i></span>
                  <input type="text" class="input-xlarge" id="username" name="username" placeholder="Username">
                </div>
              </div>
            </div>
            <div class="control-group">
              <label class="control-label">Password</label>
              <div class="controls">
                <div class="input-prepend">
                  <span class="add-on"><i class="icon-lock"></i></span>
                  <input type="Password" id="password" class="input-xlarge" name="password" placeholder="Password">
                </div>
              </div>
            </div>
            <div class="control-group">
              <label class="control-label"></label>
              <div class="controls">
                <button type="submit" class="btn btn-success login" >Log in</button>
              </div>
            </div>
          </form>
          <div id="register_result"></div>
        </div>
    </div>
    <div class="modal-footer">
      <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
    </div>
  </div>
  <!--  /Modal Register   -->

  <!--  Sounds  -->
  <div id="audio">
    <audio id="spin5sec" preload="auto">
      <source src="sounds/mp3/Spin_5sec.mp3" />
      <source src="sounds/wav/Spin_5sec.wav" />
    </audio>
    <audio id="win" preload="auto">
      <source src="sounds/mp3/Win.mp3" />
      <source src="sounds/wav/Win.wav" />
    </audio>
    <audio id="loose" preload="auto">
      <source src="sounds/mp3/Loose.mp3" />
      <source src="sounds/wav/Loose.wav" />
    </audio>
    <audio id="spinbutton" preload="auto">
      <source src="sounds/mp3/Button_SPIN_only.mp3" />
      <source src="sounds/wav/Button_SPIN_only.wav" />
    </audio>
    <audio id="buttons" preload="auto">
      <source src="sounds/mp3/Button_all_the_rest.mp3" />
      <source src="sounds/wav/Button_All_the_rest.wav" />
    </audio>
    <audio id="cashregister" preload="auto">
      <source src="sounds/mp3/Cash_Register.mp3" />
      <source src="sounds/wav/Cash_Register.wav" />
    </audio>
  </div>
  <!--  /Sounds  -->

  <script type="text/javascript">
    //const bets for bet buttons
    var MIN_BET = 1e-3;
    var MIDDLE_BET = 1e-2;
    var MAX_BET = 1e-1;
    //used for cut off bet up to third decimal
    //e.g.: Math.floor(0.0029654*MIN_BET_INVERT)*MIN_BET; // => 0.002
    var MIN_BET_INVERT = 1e3;
    function get_cookie ( cookie_name ){
      var results = document.cookie.match ( '(^|;) ?' + cookie_name + '=([^;]*)(;|$)' );
      if ( results )
        return ( unescape ( results[2] ) );
      else
        return null;
    }

    $(document).ready(function(){
      //for outputing debug info
      //DEBUG_MODE = true;
      var login_validator = new FormValidator('login_form', [{
        name: 'username',
        display: 'username',
        rules: 'required|alpha_numeric|min_length[3]|max_length[64]'
      }, {
        name: 'password',
        display: 'password',
        rules: 'required|alpha_numeric|min_length[3]|max_length[64]'
      }], function(errors, evt) {
        var SELECTOR_ERRORS = $('.error_box'),
          SELECTOR_SUCCESS = $('.success_box');
        SELECTOR_SUCCESS.hide();
        SELECTOR_ERRORS.hide();
        SELECTOR_SUCCESS.text('');
        SELECTOR_ERRORS.text('');
        //validation failed
        if (errors.length > 0) {
          SELECTOR_ERRORS.empty();
          for (var i = 0, errorLength = errors.length; i < errorLength; i++) {
            SELECTOR_ERRORS.append(errors[i].message + '<br />');
          }
          SELECTOR_SUCCESS.hide();
          SELECTOR_ERRORS.fadeIn(200);
        }
        //validation success
        else {
          SELECTOR_ERRORS.hide();
          //SELECTOR_SUCCESS.fadeIn(200);
          $('#signin').ajaxSubmit({
            success: function(result){
            //block login button until request is completed
            //$('button.login').attr('disabled', 'disabled');
            var SELECTOR_ERRORS = $('.error_box'),
            SELECTOR_SUCCESS = $('.success_box');
            $('#signin').slideUp('fast');
            //get response for signup request
            try{
              result = $.parseJSON( result );
            }
            catch(error){
              console.error(error+'| In sign in form parsing');
              SELECTOR_SUCCESS.hide();
              SELECTOR_ERRORS.fadeIn(200);
              SELECTOR_ERRORS.text('Error in request. '+error);
              $('#signin').slideDown('fast');
            }
            //success
            if ( true == result.signin_result ){
              if ( window.console ) console.log( 'result.signin_result == TRUE' );
              SELECTOR_ERRORS.hide();
              SELECTOR_SUCCESS.fadeIn(200);
              SELECTOR_SUCCESS.text('You have successfully logged');
              //clear pass
              $('#signin input#password').val('');
            }
            //output error
            else{
              SELECTOR_SUCCESS.hide();
              SELECTOR_ERRORS.fadeIn(200);
              SELECTOR_ERRORS.text(result.error_message);
              $('#signin').slideDown('fast');
            }
            //sync new values
            slot.syncWithServer().done(function(){
              slot.updateLinksAndLabel();
              //update balance
              slot.updateBalanceAndBet();
              //update bitcoin address
              slot.updateBitcoinAddress();
            });
          },
            error: function(){
              console.error('sing in form request error');
              $('#signin').slideUp('fast');
              SELECTOR_SUCCESS.hide();
              SELECTOR_ERRORS.fadeIn(200);
              SELECTOR_ERRORS.text('Error in request');
            }
          })
        }
        if (evt && evt.preventDefault) {
          evt.preventDefault();
        } else if (event) {
          event.returnValue = false;
        }
      });
        var register_validator = new FormValidator('register_form', [{
          name: 'username',
          display: 'username',
          rules: 'required|alpha_numeric|min_length[3]|max_length[64]'
        }, {
          name: 'password',
          display: 'password',
          rules: 'required|alpha_numeric|min_length[3]|max_length[64]'
        }, {
          name: 'confirm_password',
          display: 'password confirmation',
          rules: 'required|matches[password]'
        }], function(errors, evt) {
          var SELECTOR_ERRORS = $('.error_box'),
            SELECTOR_SUCCESS = $('.success_box');
          SELECTOR_SUCCESS.hide();
          SELECTOR_ERRORS.hide();
          SELECTOR_SUCCESS.text('');
          SELECTOR_ERRORS.text('');
          //validation failed
          if (errors.length > 0) {
            SELECTOR_ERRORS.empty();
            for (var i = 0, errorLength = errors.length; i < errorLength; i++) {
              SELECTOR_ERRORS.append(errors[i].message + '<br />');
            }
            SELECTOR_SUCCESS.hide();
            SELECTOR_ERRORS.fadeIn(200);
          }
          //validation success
          else {
            SELECTOR_ERRORS.hide();
            $('#signup').ajaxSubmit({
              success: function(result) {
                //block button until request is completed
                //$('button.create_my_account').attr('disabled', 'disabled');
                var SELECTOR_ERRORS = $('.error_box'),
                  SELECTOR_SUCCESS = $('.success_box');
                $('#signup').slideUp('fast');
                //get response for signup request
                try{
                  result = $.parseJSON( result );
                }
                catch(error){
                  console.error(error+'| In registration form parsing');
                  SELECTOR_SUCCESS.hide();
                  SELECTOR_ERRORS.fadeIn(200);
                  SELECTOR_ERRORS.text('Error in request. '+error);
                  $('#signup').slideDown('fast');
                }
                //success
                if ( true == result.signup_result ){
                  if ( window.console ) console.log( 'result.signup_result == TRUE' );
                  SELECTOR_ERRORS.hide();
                  SELECTOR_SUCCESS.fadeIn(200);
                  SELECTOR_SUCCESS.text('You have successfully registered');
                  //clear pass
                  $('#signup input#username').val('');
                  $('#signup input#password').val('');
                  $('#signup input#confirm_password').val('');
                  //sync new values
                }
                //output error
                else{
                  SELECTOR_SUCCESS.hide();
                  SELECTOR_ERRORS.fadeIn(200);
                  SELECTOR_ERRORS.text(result.error_message);
                  $('#signup').slideDown('fast');
                }
                slot.syncWithServer().done(function(){
                  slot.updateLinksAndLabel();
                  //update balance
                  slot.updateBalanceAndBet();
                  //update bitcoin address
                  slot.updateBitcoinAddress();
                });
              },
              error: function() {
                $('#signup').slideUp('fast');
                SELECTOR_SUCCESS.hide();
                SELECTOR_ERRORS.fadeIn(200);
                SELECTOR_ERRORS.text('Error in request');
              }
            });
          }
          if (evt && evt.preventDefault) {
            evt.preventDefault();
          } else if (event) {
            event.returnValue = false;
          }
        });

      slot = new Slot();
      slot.linesFilling();
      slot.bitcoinConnection.checkForBitcoinConnection();
      slot.checkSlotOptions();
      slot.syncWithServer().done(function() {
        slot.updateBalanceAndBet();
        slot.updateLinksAndLabel();
        //after we got user obj, we can check if he is logged
        //formValidations();
      });
      //slot.updateBalanceAndBet();
      slot.initAudio();

//BIND EVENTS---------------------------------------------------------------------------
      $('button#slots-spin').on('click', function(){
        try{
          slot.audio.spinbutton.play();
        }
        catch(e){
          if (window.console){ console.log(e+" Audio doesn't supported")}
        }
        slot.spin()
      });
      $('button#slots-maxbet').on('click', function(){
        slot.audio.buttons.play();
        slot.makeMaxBet();
      });

      $('button#make_cashout_request').on('click', function(){
        //disable OK button untill request processing is finished
        $('button#make_cashout_request').attr('disabled', 'disabled');
        //$('button#make_cashout_request').removeAttr('disabled');
        //show waiting bar
        $("div#default_confirmed_cashout_message").css("display", "block");
        //hide other
        hideAllMessages();
        slot.makeCashoutRequest();
      })

      $('a#logout_link').on('click', function(){
        $.post("AjaxRequestsProcessing.php", { slot: "signout" })
          .done(function(response) {
            try{
              response = $.parseJSON(response);
            }
            catch(error){
              if (window.console) console.error(error);
            }
            if ( response.signout_result == true ){
              //after user was synced...
              slot.syncWithServer().done(function(){
                //clear success/error messages
                var SELECTOR_ERRORS = $('.error_box'),
                  SELECTOR_SUCCESS = $('.success_box');
                SELECTOR_SUCCESS.css({ display: 'none' });
                SELECTOR_ERRORS.css({ display: 'none' });
                //...set proper link (login/logout/register) and proper form (login/logout/register)
                slot.updateLinksAndLabel();
              });
            }
            else{
              if (window.console) console.error(response.error_message);
            }
          })
      })

      $('div#slots-body').bind('spinFinished', function(e, status, param2) {
        if (slot.autoplay == true){
          autoplay();
        }
        // unpress autoplay button when autoplay is false
        else{
          $('button#slots-autoplay').removeClass('active');
        }
      });

      $('button#slots-autoplay').bind('autoplayTrigger', function(e, status, param2) {
      });
      $('div#slots-body').bind('newIncommingPayment', function(e, status, param2) {
        if (window.console) console.log('newIncommingPayment = '+status);
      });

      //when response for spin is getting
      $('div#slots-body').bind('responseSpinGetting', function(e, successStatus, param2) {
        if (successStatus){
          slot.afterSpinResponseGetting();
        }
      });

//AUTOPLAY---------------------------------------------------------------------------
      function autoplay(){
        if ( !slot.spin() ){
          slot.autoplay = false;
          $('button#slots-autoplay').removeClass('active');
          return false;
        }
      }

      $('button#slots-autoplay').on('click', function(){
        slot.audio.buttons.play();
        if ( user.is_registered && !user.is_logged ){
          slot.statusDisplay.show( slot.statusDisplay.messages.registeredAndNotLoggedCashoutAndPlay, 15 );
          return false;
        }
        if ( slot.autoplay == true ){//toggle to false
          $('button#slots-autoplay').removeClass('active');
          slot.autoplay = false;
          return;
        }
        else{//toggle to true
          $('button#slots-autoplay').addClass('active');
          slot.autoplay = true;
        }
        autoplay();
      });
      $('button#slots-lastbet').on('click', function(){
        slot.audio.buttons.play();
        slot.setBetTo(slot.getLastBet());
      });
      //plus
      $('button.slots-plus').on('click', function(){
        slot.audio.buttons.play();
        var buttonPlusId = this.id;
        switch(buttonPlusId){
          case 'slots-plus001':
            slot.incBetTo(MIN_BET);
            break;
          case 'slots-plus01':
            slot.incBetTo(MIDDLE_BET);
            break;
          case 'slots-plus1':
            slot.incBetTo(MAX_BET);
            break;
        }
      });
      //minus
      $('button.slots-minus').on('click', function(){
        slot.audio.buttons.play();
        var buttonPlusId = this.id;
        switch(buttonPlusId){
          case 'slots-minus001':
            //slot.decBetTo(0.001);
           slot.decBetTo(MIN_BET);
            break;
          case 'slots-minus01':
            //slot.decBetTo(0.01);
           slot.decBetTo(MIDDLE_BET);
            break;
          case 'slots-minus1':
            //slot.decBetTo(0.1);
           slot.decBetTo(MAX_BET);
            break;
        }
      });
      function hideAllMessages (){
        $("div#confirmed_cashout_message").css("display", "none");
        $("div#not_confirmed_cashout_message").css("display", "none");
        $("div#zero_cashout_message").css("display", "none");
        $("div#paying_out_off_message").css("display", "none");
        $("div#big_money_message").css("display", "none");
        $("div#spin_is_running_cashout_message").css("display", "none");
        $("div#no_bitcoin_connection_message").css("display", "none");
        $("div#wrong_bitcoin_address").css("display", "none");
      }

//------------------------------------CASH OUT------------------------------------------------------------------------
      $('button#slots-cashout').on('click', function(){
        slot.audio.buttons.play();
        //user registered but not logged in
        if ( user.is_registered && !user.is_logged ){
          slot.statusDisplay.show( slot.statusDisplay.messages.registeredAndNotLoggedCashoutAndPlay, 15 );
          return false;
        }
        //if slot.currentUserBalance becomes 0 -- update label
        slot.updateLinksAndLabel();
        slot.checkSlotOptions();
        if (window.console) console.log('-==Start cashing out==-');
        $("div#default_confirmed_cashout_message").css("display", "block");
        $("div#user_own_bitcoin_address").css("display", "none");
        //... and hide others
        hideAllMessages();
        slot.bitcoinConnection.checkForBitcoinConnection().always(function() {
          //hide OK if balacne is zero OR no connection
          if ( slot.currentUserBalance == 0 || !slot.bitcoinConnection.lastCheck ){
            $("button#make_cashout_request").hide();
          }
          //show OK button
          else{
            $("button#make_cashout_request").show();
          }
          //do the connection checking in .done(function...) because we need to get
          //response from bitcoind first
          if (window.console) console.log('---checkForBitcoinConnection---');
          $("div#default_confirmed_cashout_message").css("display", "none");
          if ( !slot.bitcoinConnection.newCheck ){
            $("div#no_bitcoin_connection_message").css("display", "block");
            return false;
          }
          $("div#user_own_bitcoin_address").css("display", "block");

          // if we haven't entered address
          if ( user.user_wallet_for_registered_user == "" || !user.is_logged ){
            //sends to the one the user send the money from
            $('input#own_address').val(user.user_wallet);
          }
          else if ( user.is_logged ){
            $('input#own_address').val(user.user_wallet_for_registered_user);
          }
          else{
            $('input#own_address').val(user.user_wallet);
          }
          //user.user_wallet_for_registered_user = $("input#own_address").val();
          //user can paste his own bitcoin address if he had registered and logged in
          if ( user.is_logged && user.is_registered /*&& slot.currentUserBalance > 0*/ ){
            $('input#own_address').removeAttr('readonly');
          }
          else{
            $('input#own_address').attr('readonly', 'readonly');
          }
        });
      });

//------------------------------------VERIFY BUTTON------------------------------------------------------------------------
      //verify seeds and symbols
      $('button#verify_button').on('click', function(){
        //1) check that rand symbols match to symbols for given client and server seeds
        var seeds = new Array();
        seeds['clientSeed'] = $('div#verify input#client_seed').val();//slot.getClientSeed();
        seeds['serverSeeds'] = $('div#verify input#server_seed').val();
        var symbolsForChecking = $('div#verify input#symbols').val();
        //check all seeds are not empty and animate them if they are
        if (seeds['clientSeed'].length == 0){//no string
          $('div#verify input#client_seed').css('opacity',0.5).stop().animate({opacity: 1}, 300);
          return false;
        }
        if (seeds['serverSeeds'].length == 0){
          $('div#verify input#server_seed').css('opacity',0.5).stop().animate({opacity: 1}, 300);
          return false;
        }
        if (symbolsForChecking.length == 0){
          $('div#verify input#symbols').css('opacity',0.5).stop().animate({opacity: 1}, 300);
          return false;
        }
        var arrServerSeeds = null;
        //serverSeeds should be an array
        try{
          //var arrServerSeeds = eval( "("+seeds['serverSeeds']+")");
          arrServerSeeds = $.parseJSON(seeds['serverSeeds']);
        }
        catch(e){
          if (window.console) console.fail('Error '+e+'. arrServerSeeds = '+arrServerSeeds);
        }
        // realSymbols in JSON format
        var realSymbols = slot.calcSymbolsFromClientSeedAndServerSeeds(seeds['clientSeed'], arrServerSeeds);
        var animateSpeed = 2000;
        var warningColor = '#AD3A3A';
        var successColor = '#3AAD50';
        var seedsSuccessColor = '#3A87AD';
        //mismatch
        if (symbolsForChecking != realSymbols){
          if (window.console) console.log('symbolsForChecking != realSymbols');
          $('div#verify input#symbols').css('background', warningColor).stop().animate({backgroundColor : '#FFF'}, animateSpeed);
        }
        else if (symbolsForChecking == realSymbols){
          if (window.console) console.log('symbolsForChecking == realSymbols');
          $('div#verify input#symbols').css('background', successColor).stop().animate({backgroundColor : '#FFF'}, animateSpeed);
        }
        //2) check that Hash(server seed) == Hash (raw Server seed)
        var jsonServerSeed =  $('div#verify input#server_seed').val();
        var hashedServerSeed = $('div#verify input#last_hashed_server_seed').val();
        //check hashed and unhashed for matching
        if (slot.isUnhashedAndHashedSeedsMatch(jsonServerSeed, hashedServerSeed)){
          if (window.console) console.log('isUnhashedAndHashedSeedsMatch == true');
          $('div#verify input#server_seed').css('background', seedsSuccessColor).stop().animate({backgroundColor : '#FFF'}, animateSpeed);
          $('div#verify input#last_hashed_server_seed').css('background', seedsSuccessColor).stop().animate({backgroundColor : '#FFF'}, animateSpeed);
        }
        //mismatch
        else{
          if (window.console) console.log('isUnhashedAndHashedSeedsMatch == false');
          $('div#verify input#server_seed').css('background', warningColor).stop().animate({backgroundColor : '#FFF'}, animateSpeed);
          $('div#verify input#last_hashed_server_seed').css('background', warningColor).stop().animate({backgroundColor : '#FFF'}, animateSpeed);
        }
      });
      //run once
      slot.getHashedServerSeeds();
      slot.getClientSeed();
      //requests per interval
      setInterval(slot.checkSlotOptions, slot.timeouts.checkSlotOptionsInterval);
      setInterval(slot.checkForNewIncommingPayment, slot.timeouts.checkForNewIncommingPaymentInterval);
      setInterval(slot.bitcoinConnection.checkForBitcoinConnection, slot.timeouts.bitcoinCheckConnectionInterval);
    });
  </script>
</body>
</html>