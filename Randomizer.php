<?php
/**
 * Description of Randomizer
 *
 * @author vadim24816
 */
require_once 'Appconfig.php';

//js and php (mt_rand) MTs are return different values
//... this php MT implementation returns the same values like js one
require_once 'mersenne_twister.php';
use mersenne_twister\twister;

class Randomizer {
  //make it singletone
  protected static $randomizer;
  private function __construct(){}
  public function __destruct(){}
  private function __clone(){} 
  //private function __wakeup(){}
  public static function get_instance(){
    if (is_null(self::$randomizer)){
      self::$randomizer = new Randomizer();
      return self::$randomizer;
    }
    return self::$randomizer;
  }
  
  //reinit generator of mt_rand() using mt_srand()
  public function mt_srand($seed = null){
    if ($seed === null){
      mt_srand();
    }
    else{
      mt_srand($seed);
    }
  }
  //return new random number using mt_rand()
  public function mt_rand(){
    $rand_num = mt_rand(0, 63);
    //$rand_num = mt_rand();
    return $rand_num;
  }
  
  /**
   * return rand number from 0 to 63
   */
  public function mersenne_twister_int32($seed = null){
    if ($seed === null){
      $twister_32 = new twister();
    }
    else{
      $twister_32 = new twister($seed);
    }
    //return $twister_32->rangeint(0, 63);
    return $twister_32->int32();
  }
}