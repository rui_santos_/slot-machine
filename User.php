<?php
require_once 'Appconfig.php';

/**
 * Description of User
 *
 * @author vadim24816
 */

class User {
  //make it singletone
  
  protected static $user;
  private function __construct(){}
  public function __destruct(){}
  private function __clone(){} 
  public static function get_instance(){
    if (is_null( self::$user) ){
      self::$user = new User();
      self::$user->auth();
      return self::$user;
    }
    return self::$user;
  }
  
  public
    //everyone gets
    $uid,
    //for user who was registered
    $username,
    $md5_password,
    $is_logged,
    $is_registered,
    //is not used
    $phpsessid,
    //from where user sent money for playing
    $user_wallet,
    //registered user paste his own address
    $user_wallet_for_registered_user,
    //to where user should send money for playing
    $bitcoin_receive_address,
    //current balance
    $money_balance,
    $affiliateusername,
    $remote_user_address,
    //last time of caching
    $last_check_time,
    //cached from bitcoin
    $bitcoin_money_balance,
    //allowed tabs
    $allowed_tabs,
    //limit tabs for the same user
    $OPENED_TABS_LIMIT;

  //real escape User obj for DB
  public function real_escape(){
    $db = DBconfig::get_instance();
    //escaping party
    $this->uid = $db->mysqli_link->real_escape_string($this->uid);
    $this->username = $db->mysqli_link->real_escape_string($this->username);
    $this->md5_password = $db->mysqli_link->real_escape_string($this->md5_password);
    $this->phpsessid = $db->mysqli_link->real_escape_string($this->phpsessid);
    $this->user_wallet = $db->mysqli_link->real_escape_string($this->user_wallet);
    $this->user_wallet_for_registered_user = $db->mysqli_link->real_escape_string($this->user_wallet_for_registered_user);
    $this->bitcoin_receive_address = $db->mysqli_link->real_escape_string($this->bitcoin_receive_address);
    $this->money_balance = $db->mysqli_link->real_escape_string($this->money_balance);
    $this->affiliateusername = $db->mysqli_link->real_escape_string($this->affiliateusername);
    $this->remote_user_address = $db->mysqli_link->real_escape_string($this->remote_user_address);
    $this->last_check_time = $db->mysqli_link->real_escape_string($this->last_check_time);
    $this->bitcoin_money_balance = $db->mysqli_link->real_escape_string($this->bitcoin_money_balance);
  }

  public static function match_to_regexp( $pattern, $value ){
    if ( !preg_match($pattern, $value ) ){
      return false;
    }
    else{
      return true;
    }
  }

  public static function is_valid_phpsessid( $phpsessid ){
    $phpsessid_pattern = '/^[a-zA-Z0-9]{10,32}$/';
    return User::match_to_regexp($phpsessid_pattern, $phpsessid);
  }

  public static function is_valid_uid( $uid ){
    $uid_pattern = '/^[a-zA-Z0-9]{40}$/';
    return User::match_to_regexp($uid_pattern, $uid);
  }

  public function auth(){
    $file = '';
    $line = '';
    //check for whether headers were already sent
    if ( headers_sent( $file, $line ) ){
      $error_message = "Headers were already sent in ".'file: '.$file.'line: '.$line;
      error_log( $error_message, 0 );
    }
    //1) user visits first time OR 2) if uid hadn't found in db?
    //user hasn't registered yet
    if ( empty( $_COOKIE['uid'] ) ||
         !User::is_valid_uid( $_COOKIE['uid'] ) ||
         !User::is_valid_phpsessid( $_COOKIE['PHPSESSID'] )
    ){
      $this->logout();
      try{
        $this->auto_reg();
      }
      catch ( Exception $e ){
        $error_message = "Auth failed. File ".__FILE__." Line " .__LINE__;
        $error_message .= $e->getTraceAsString();
        error_log( $error_message, 0 );
      }
    }
    //user has registered
    else{
      $uid = $_COOKIE['uid'];
      $this->uid = $uid;
      $this->OPENED_TABS_LIMIT = 4;
      //let to user limit tabs
      for( $tab_num = 0; $tab_num < $this->OPENED_TABS_LIMIT; $tab_num++  ){
        $this->allowed_tabs[$tab_num] = uniqid('tab_num_'.$tab_num.'_');
      }
      //search for user by given uid
      if ($this->get_from_db($uid)){
      }
      //not logged in by default
      $this->is_logged = false;
      //check if user already registered
      $this->is_registered = $this->is_registered() ? true : false;
      //if bitcoin_receive_address didn't set, set it
      if ( empty($this->bitcoin_receive_address) ){
        $this->set_bitcoin_receive_address();
        //and write changes to DB
        $this->save_in_db();
      }
    }
    return $this;
  }

  public function logout(){
    //clear session and cookie
    SetCookie("uid", "");
    SetCookie("PHPSESSID", "");
    session_unset();
    session_destroy();
    session_id(null);
  }

  public function set_bitcoin_receive_address(){
    $mbc = MyBitcoinClient::get_instance();
    if ( $mbc->check_last_connect() == true ){
      try{//to get bitcoin_receive_address
        $this->bitcoin_receive_address = $mbc->getaccountaddress($this->uid);
      }
      catch (BitcoinClientException $e) {
        //dump_it($e->getTraceAsString());
        $error_message = "Set bitcoin address failed. Class ".__CLASS__." Method ".__METHOD__." File ".__FILE__." Line " .__LINE__;
        $error_message .= $e->getTraceAsString();
        error_log( $error_message, 0 );
      }
    }
  }

  //check by username
  /**
   * @param $username
   * @return bool when no user exists
   * @return $user['uid'] when user exists
   */
  public static function is_username_already_exists( $username ){
    if ( $username == '' ){
      return false;
    }
    $db = DBconfig::get_instance();
    $username = $db->mysqli_link->real_escape_string( $username );
    $user = $db->mysqli_fetch_array('SELECT * FROM users WHERE username = \''.$username.'\'');
    //if there is no user with given uid
    if ( is_array( $user ) ){
      return $user['uid'];
    }
    //the user is found
    else{
      return false;
    }
  }

  public static function are_username_and_password_valid( $username, $password, $confirm_password ){
    //check username and pass for alphanumeric characters
    if ( !ctype_alnum($username) || !ctype_alnum($password) ){
      return false;
    }
    //check length
    if(
      ( strlen( $username ) < 3 ||  strlen( $username ) > 64 ) ||
      ( strlen( $password ) < 3 || strlen( $password ) > 64 )
    ){
      return false;
    }
    //match passwords
    if ( $password != $confirm_password ){
      return false;
    }
    //all right
    return true;
  }

  public function manual_login( $username, $password ){
    //update user
    $this->update_from_db();
    if ( false === ($uid = User::is_username_already_exists($username)) ){
      return -1;
    }
    $user = User::static_get_from_db( $uid );
    //match
    if (
      $user->username === $username &&
      $user->md5_password === md5( $password )
    ){
      //filling current user
      $this->get_from_db( $uid );
      //set cookies
      $file = '';
      $line = '';
      //check for whether headers was already sent
      if (headers_sent($file, $line)){
        $error_message = "Headers were  already sent in ".'file: '.$file.'line: '.$line;
        error_log( $error_message, 0 );
      }
      SetCookie("uid", $this->uid, AppConfig::now_plus_x_years(), '/', /*'.bitbandit.eu'*/AppConfig::$domainname, true, false);
      $this->is_registered = true;
      $this->is_logged = true;
      return true;
    }
    //not match and user wasn't logged in yet
    elseif( !$this->is_logged ){
      $this->is_logged = false;
      return false;
    }
  }

  public function manual_logout(){
    $this->is_logged = false;
    return true;
  }

  //if user wishes he can reg using username and password
  public function manual_reg( $username, $password, $confirm_password ){
    //checks
    if ( User::is_username_already_exists( $username ) ){
      //username already exists, need another username
      return -1;
    }
    if ( !$this->are_username_and_password_valid( $username, $password, $confirm_password ) ){
      //username or password/confirm_password not valid
      return -2;
    }
    //if registered user try to reg, create new user
    if ( $this->is_registered() ){
      //make auto reg
      $this->auto_reg();
    }
    //and then save username and pass
    $this->username = $username;
    $this->md5_password = md5( $password );
    $this->is_registered = true;
    if ( $this->save_in_db() ){
      return true;
    }
    else{
      return false;
    }
  }

  //user was manual_reg or just auto got uid
  public function is_registered(){
    $this->update_from_db();
    //user is registered when there is his name in DB
    if ( strlen( $this->username ) > 0 ){
      $this->is_registered = true;
      return true;
    }
    else{
      $this->is_registered = false;
      return false;
    }
  }

  //user registered but not logged in
  public function is_registered_and_not_logged(){
    if ( $this->is_registered() && !$this->is_logged ){
      return true;
    }
    else{
      return false;
    }
  }

  public function get_affiliateusername_from_link(){
    try{
      if ( empty( $_GET['r'] ) ){
        //throw new Exception('empty_r error');
        return 'empty_r';
      }
      $affiliateusername = $_GET['r'];
      $affiliateusername_pattern = '/^[a-zA-Z0-9]{3,64}$/';
      //if affiliateusername is provided
      //and affiliateusername should already exist in users table
      if (
        preg_match( $affiliateusername_pattern, $affiliateusername ) &&
        User::is_username_already_exists( $affiliateusername )
      ){
        return $affiliateusername;
      }
      else{
        return 'not_pregmatch_or_not_exists';
      }
    }
    catch ( Exception $e ){
      //dump_it($e->getTraceAsString());
      $error_message = "get_affiliateusername_from_link error. File ".__FILE__." Line " .__LINE__;
      $error_message .= $e->getTraceAsString();
      $error_message .= $_GET ;
      error_log( $error_message, 0 );
    }
  }

  //user is regged automaticly after first visit
  public function auto_reg(){
    if (!empty($_SERVER['REMOTE_ADDR'])){
      $remote_user_address = $_SERVER['REMOTE_ADDR'];
    }
    $this->uid = sha1(uniqid(""));
    $this->money_balance = 0;

    //user can sign up if he wish
    $this->username = '';
    $this->md5_password = '';
    $this->is_logged = false;
    $this->is_registered = false;

    //try to get from bitcoin and set in User address
    $this->set_bitcoin_receive_address();
    
    $this->user_wallet = '';
    $this->user_wallet_for_registered_user = '';

    $this->affiliateusername = $this->get_affiliateusername_from_link();
    //if someone entered by affiliate link - create link in affiliate table
    $affiliate_link = new AffiliateLink( $this->affiliateusername, $this->uid );
    //and save it
    $affiliate_link->save_affiliate_link();

    $this->remote_user_address = $remote_user_address;
    //e.g.:                  04:34:19 11.11.2012
    $this->created_at = date('h:i:s d.m.Y');
    $this->bitcoin_money_balance = 0;
    if ($this->save_in_db()){
      $file = '';
      $line = '';
      //check for whether headers were already sent
      if ( headers_sent( $file, $line ) ){
        $error_message = "Headers were  already sent in ".'file: '.$file.' line: '.$line;
        error_log( $error_message, 0 );
      }
      SetCookie("uid", $this->uid, AppConfig::now_plus_x_years(), '/', /*'.bitbandit.eu'*/AppConfig::$domainname, true, false);
      return true;
    }
    else {
      //throw new Exception('Failed to add to the database.');
      $error_message = "Failed to add user to the database. File ".__FILE__." Line " .__LINE__;
      error_log( $error_message, 0 );
      return false;
    }
  }

  static function static_get_from_db( $uid ){
    $user = new User();
    try{
      $db = DBconfig::get_instance();
      $uid = $db->mysqli_link->real_escape_string( $uid );
      $user_arr = $db->mysqli_fetch_array('SELECT * FROM users WHERE uid = \''.$uid.'\'');
    }
    catch ( Exception $e ){
      //dump_it($e->getTraceAsString());
      $error_message = "Failed to get user from DB. File ".__FILE__." Line " .__LINE__;
      $error_message .= $e->getTraceAsString();
      error_log( $error_message, 0 );
    }
    //if there is no user with given uid
    if ( $user_arr == FALSE ){
      return FALSE;
    }
    //the user is found
    else{
      $user->uid = $uid;
      $user->username = $user_arr['username'];
      $user->md5_password = $user_arr['password'];
      $user->bitcoin_receive_address = $user_arr['bitcoin_receive_address'];
      $user->money_balance = $user_arr['money_balance'];
      $user->user_wallet = $user_arr['user_wallet'];
      $user->user_wallet_for_registered_user = $user_arr['user_wallet_for_registered_user'];
      $user->remote_user_address = $user_arr['remote_user_address'];
      $user->affiliateusername = $user_arr['affiliateusername'];
      $user->last_check_time = $user_arr['last_check_time'];
      $user->bitcoin_money_balance = $user_arr['bitcoin_money_balance'];
      return $user;
    }
  }

  //get user record from db
  function get_from_db($uid){
    try{
      $db = DBconfig::get_instance();
      $uid = $db->mysqli_link->real_escape_string($uid);
      $user = $db->mysqli_fetch_array('SELECT * FROM users WHERE uid = \''.$uid.'\'');
    }
    catch ( Exception $e ){
      $error_message = "Failed to get user from DB. File ".__FILE__." Line " .__LINE__;
      $error_message .= $e->getTraceAsString();
      error_log( $error_message, 0 );
    }
    //if there is no user with given uid
    if ($user == FALSE){
      return FALSE;
    }
    //the user is found
    else{
      $this->uid = $uid;
      $this->username = $user['username'];
      $this->md5_password = $user['password'];
      $this->bitcoin_receive_address = $user['bitcoin_receive_address'];
      $this->money_balance = $user['money_balance'];
      $this->user_wallet = $user['user_wallet'];
      $this->user_wallet_for_registered_user = $user['user_wallet_for_registered_user'];
      $this->remote_user_address = $user['remote_user_address'];
      $this->affiliateusername = $user['affiliateusername'];
      $this->last_check_time = $user['last_check_time'];
      $this->bitcoin_money_balance = $user['bitcoin_money_balance'];
      return TRUE;
    }
  }

  function is_user_exist($uid){
    $db = DBconfig::get_instance();
    $user = $db->mysqli_fetch_array('SELECT * FROM users WHERE uid = \''.$uid.'\'');
    //if there is no user with given uid
    if ($user == FALSE){
      return FALSE;
    }
    //the user is found
    else{
      return TRUE;
    }
  }

  //for updating money balance
  function update_from_db(){
    $this->get_from_db($this->uid);
  }

  function save_in_db(){
    try{
      $db = DBconfig::get_instance();
      $this->real_escape();

      //if user not exist insert in DB
      if (!$this->is_user_exist($this->uid)){
        $res = $db->query( "INSERT INTO users (uid, username, password, bitcoin_receive_address, user_wallet, user_wallet_for_registered_user, money_balance, affiliateusername, created_at, remote_user_address, last_check_time, bitcoin_money_balance)
          VALUES ('$this->uid', '$this->username', '$this->md5_password', '$this->bitcoin_receive_address', '$this->user_wallet', '$this->user_wallet_for_registered_user', '$this->money_balance', '$this->affiliateusername', NOW(), '$this->remote_user_address', '$this->last_check_time', '$this->bitcoin_money_balance' )");//NOW() == '$this->created_at',
      }
      //if user exists already just update record
      else {
        $res = $db->query( "UPDATE users SET
          `username` = '$this->username',
          `password` = '$this->md5_password',
          `bitcoin_receive_address` = '$this->bitcoin_receive_address',
          `user_wallet` = '$this->user_wallet',
          `user_wallet_for_registered_user` = '$this->user_wallet_for_registered_user',
          `money_balance` = '$this->money_balance',
          `affiliateusername` = '$this->affiliateusername',
          `remote_user_address` = '$this->remote_user_address',
          `last_check_time` = '$this->last_check_time',
          `bitcoin_money_balance` = '$this->bitcoin_money_balance'
          WHERE `uid` = '$this->uid'
        ");
      }
      if ( !$res ){
        //echo 'no res';
        $error_message = "Failed to save into DB. File ".__FILE__." Line " .__LINE__;
        error_log( $error_message, 0 );
        return FALSE;
      }
    }
    catch ( Exception $e ) {
      //dump_it($e->getTraceAsString());
      $error_message = "Failed to save into DB. File ".__FILE__." Line " .__LINE__;
      $error_message .= $e->getTraceAsString();
      error_log( $error_message, 0 );
    }
    //update user after saving
    $this->get_from_db($this->uid);
    return true;
  }
  
  // return address from which user sent money
  public function get_user_wallet_by_uid(){
    try{
      $db = DBconfig::get_instance();
      $b = MyBitcoinClient::get_instance();
      $this->uid = $db->mysqli_link->real_escape_string( $this->uid );
      $transactions_list = $b->query("listtransactions", $this->uid, "10", "0");
      // 0, null, false
      if (!$transactions_list){
        //'Error. Bitcoin hasn\'t transactions for given user id';
        $error_message = "[Warning] in get_user_wallet_by_uid(). Bitcoin hasn't transactions for given user id. uid = ".$this->uid.". File ".__FILE__." Line " .__LINE__;
        error_log( $error_message, 0 );
        return false;
      }
      $transactions_list_size = count($transactions_list);
      //exactly 1 transcation - where category should be 'receive'
      if ($transactions_list_size == 1 && isset($transactions_list[0]['receive'])){
        $txid = $transactions_list[0]["txid"];
      }
      //if count of transactions in $transactions_list are great than 1
      //start to search transaction where 'category' == 'receive'
      if ($transactions_list_size > 1){
        for($i = 0; $i < $transactions_list_size; $i++){
          //if transaction has 'category' and 'txid'
          if (!empty($transactions_list[$i]['category']) && !empty($transactions_list[$i]['txid'])){
            $txid = $transactions_list[$i]['txid'];
          }
          foreach ($transactions_list[$i] as $key => $value) {
            if ($key === 'receive'){
              $txid = $transactions_list[0]["txid"];
            }
          }
        }
      }
      //if there are no transactions with 'receive' category
      if (!isset($txid)){
        $error_message = "[Warning] in get_user_wallet_by_uid().  there are no transactions with 'receive' category. File ".__FILE__." Line " .__LINE__;
        error_log( $error_message, 0 );
        return false;
      }
      $raw_transaction_arr = $b->query("getrawtransaction", $txid, "1");
      if (empty($raw_transaction_arr['vout'])){
        //echo 'User\'s bitcoin wallet address not found.';
        $error_message = "[Warning] in get_user_wallet_by_uid(). User's bitcoin wallet address not found.. File ".__FILE__." Line " .__LINE__;
        error_log( $error_message, 0 );
        return false;
      }
      $user_wallet_addresses = array();
      //user's address placed in 'vout'
      for($i = 0; $i < count($raw_transaction_arr['vout']); $i++){
        //search 'addresses' array in 'scriptPubKey' array
        if (!empty($raw_transaction_arr['vout'][$i]['scriptPubKey']) && !empty($raw_transaction_arr['vout'][$i]['scriptPubKey']['addresses'])){
          //collect ALL possible user wallet addresses in $possible_addresses
          $possible_addresses = $raw_transaction_arr['vout'][$i]['scriptPubKey']['addresses'];
          for ($address_iterator = 0;$address_iterator<count($possible_addresses);$address_iterator++){
            //add new user wallet address to addresses array
            array_push($user_wallet_addresses, $possible_addresses[$address_iterator]);
          }
        }
      }
      $user_wallet_address = '';
      //search user wallet address,
      //condition: it must be not equal(!) to receive address
      for ($i=0;$i<count($user_wallet_addresses);$i++){
        if ( $user_wallet_addresses[$i] != $this->bitcoin_receive_address ){
          $user_wallet_address = $user_wallet_addresses[$i];
          break;
        }
      }
      $address_and_txid = array( 'user_wallet_address' => $user_wallet_address, 'txid' => $txid );
      return $address_and_txid;
    }
    catch ( Exception $e ) {
      //dump_it($e->getTraceAsString());
      $error_message = "Failed to get_user_wallet_by_uid. File ".__FILE__." Line " .__LINE__;
      $error_message .= $e->getTraceAsString();
      error_log( $error_message, 0 );
    }
  }

  private function is_update_time_interval_expired(){
    $db = DBconfig::get_instance();
    $query = 'SELECT `id`, `last_check_status` ,  `check_time_interval`  FROM time_intervals WHERE id = "incoming_payment"';
    $res = $db->query($query);
    if ( !$res )
      return -1;
    $time_interval_row_from_db = $db->mysqli_fetch_array_by_result($res);
    if ( (microtime(true) - $this->last_check_time ) > $time_interval_row_from_db['check_time_interval']){
      return true;
    }
    else{
      return false;
    }
  }

  //one function for cash_in and cash_out
  public function cash_move($to_where = 'in', $amount = null, $cashout_to_given_address = false, $given_address = '' ){
    if ($to_where !== 'in' && $to_where !== 'out'){
      return array( "amount" => -1, "txid" => -1 );
    }
    $mbc = $_SESSION['MyBitcoinClient'];
    $user = $this;
    //get new connection checking
    if ( !$mbc->check_last_connect() ){
      //return -2;
      return array( "amount" => -2, "txid" => -2 );
    }
    
    //payments processing 
    //----------------------------------IN-----------------------------------------------------------------------------
    if ($to_where == 'in'){
      $db = DBconfig::get_instance();
      //0. get updated user from DB (otherwise cash_in func rewrites anything we changed for user in DB)
      $user->update_from_db();
      //1. check if time update interval is expired
      if ( $this->is_update_time_interval_expired() ){
        //2. expired - get from bitcoin(!): $user_bitcoin_money_balance = $mbc->getbalance($user->uid, 0);
        $user_bitcoin_money_balance = $mbc->getbalance( $user->uid, 0 );
        //true - payment was sent by user to bitcoin
        //false - payment was entered in DB by admin and got from it OR payment is cashback
        $get_payment_from_bitcoin = true;
        //keep last_check_time of money balance for every(!) user
        $user->last_check_time = microtime( true );
      }
      else{
        //3. not expired - get cached(!) value from DB: $user_bitcoin_money_balance = $user->bitcoin_money_balance
        //$user->update_from_db();
        $user_bitcoin_money_balance = $user->bitcoin_money_balance;// $this->money_balance;
        //remove new payment in DB, that we cached from bitcoin
        $user->bitcoin_money_balance = 0;
        $get_payment_from_bitcoin = false;
      }
      //save all changes to DB
      $user->save_in_db();

      /*
      //for 'In' payments we use bitcoind(!) user balance
      $user_bitcoin_money_balance = $mbc->getbalance($user->uid, 0);
      */
      if ( $user_bitcoin_money_balance == 0 ){
        return array( "amount" => 0, "txid" => 0 );
      }
      //no in/outcoming payments was made
      if ( $user_bitcoin_money_balance < 0 ){
        //return -3;
        return array( "amount" => -3, "txid" => -3 );
      }
      //move money which was sent by user to the common slot account
      //(!) Because we move uncomfirmed money (with 0 confirmations and we need 2)
      //... user balance (in bitcoin) will be negative 
      //... until the moving will be confirmed

      //if payment was received from bitcoin, we process it in bitcoin
      if ( $get_payment_from_bitcoin ){
        $mbc->check_and_move( $user->uid, Slot::$bitcoin_account_name/*'SlotBank'*/, $user_bitcoin_money_balance, 0/*Appconfig::$min_confirmations_for_cash_out/* == 2*/,'Move from the user account '.$user->uid.' to the common slot bitcoin account '.Slot::$bitcoin_account_name.' Money: '.$user->money_balance );
      }
      //else continue

      //ADD (+=) new payment to user balance
      $user->money_balance += $user_bitcoin_money_balance;
      //if payment was received from bitcoin
      if ( $get_payment_from_bitcoin ){
        //find out user's bitcoin wallet address
        $user_walllet_address_and_txid = $user->get_user_wallet_by_uid();
        $wallet_from_which_user_sent_money = $user_walllet_address_and_txid['user_wallet_address'];
        //new and prev addresses are invalid
        if ( !MyBitcoinClient::is_valid_bitcoin_address( $wallet_from_which_user_sent_money )
            && !MyBitcoinClient::is_valid_bitcoin_address( $user->user_wallet )
        ){
          $wallet_from_which_user_sent_money = '';
        }
        //new address is invalid, prev address is ok
        elseif ( !MyBitcoinClient::is_valid_bitcoin_address( $wallet_from_which_user_sent_money )
            && MyBitcoinClient::is_valid_bitcoin_address( $user->user_wallet )
        ){
          //just keep prev address
          $wallet_from_which_user_sent_money = $user->user_wallet;
      }
        elseif( MyBitcoinClient::is_valid_bitcoin_address( $wallet_from_which_user_sent_money )
          || MyBitcoinClient::is_valid_bitcoin_address( $user->user_wallet )
        ){
          //get new address
          $wallet_from_which_user_sent_money = $wallet_from_which_user_sent_money;
        }
        //find out the input transaction txid
        $txid = $user_walllet_address_and_txid['txid'];
        //money was received as deposit
        $deposit = 1;
      }
      // if transaction/payment received from DB, txid = ''
        else{
        //$txid = 'referral_fee_'.sha1( uniqid( ) );
        $txid = '';
        $wallet_from_which_user_sent_money = '';
        //money was received as cashback bonus
        $deposit = 2;
      }
      //( txid, all money, deposit = true, uid )
      $t = new Transaction( $txid, $user_bitcoin_money_balance, $deposit, $user->uid );
      //keep amount of money was moved for returning
      $amount_of_money_was_moved = $user_bitcoin_money_balance;
      //should be not 0 if money was received (there is transaction which has 'received' category)
      if ( $wallet_from_which_user_sent_money !== 0 ){
        //if it is real (not added from DB) payment from bitcoin network, save user's address
        if ( $get_payment_from_bitcoin ){
          $user->user_wallet = $wallet_from_which_user_sent_money;
        }
      }
      else{
        //money was not really received 
        return array( "amount" => -4, "txid" => -4 );
      }
    }
    //----------------------------------------OUT----------------------------------------------------------------------
    if ( $to_where == 'out' ){
      //user hasn't logged in inaccessible to cash out money
      if ( $user->is_registered_and_not_logged() ){
        return array( "amount" => -401, "txid" => -401 );
      }
      //check options are not off for money out
      $options = Slot::get_option_from_db();
      if ( $options['paying_out']['option_value'] == 'off' ){
        return array( "amount" => -200, "txid" => -200 );
      }
      //nothing to out
      if ( $user->money_balance <= 0 || $amount == null || $amount <= 0 ){
        return array( "amount" => 0, "txid" => 0 );
      }
      //check that money which user deposited are confirmed (min_conf = 2)
      //this means that user balance with 2 confirmations should be 0
      if ( $mbc->getbalance( $user->uid, Appconfig::$min_confirmations_for_cash_out /* == 2*/ ) < 0 ){
        //not enough confirmations
        return array( "amount" => -101, "txid" => -101 );
      }
      
      //when the cashout amount is more than the wallet amount
      if ( /*$user->money_balance*/ $amount > $mbc->getbalance( NULL,$min_conf = 6) ){
        //not enough confirmations
        return array( "amount" => -500, "txid" => -500 );
      }
      
      //1)for cash out only whats on balance not balance+bet or 2) amount not got
      if ( $amount > $user->money_balance ){
        //amount for cashout > user money balance
        return array( "amount" => -500, "txid" => -500 );
      }

      // to entered by user address
      if ( $cashout_to_given_address === true ){
        if ( MyBitcoinClient::is_valid_bitcoin_address( $given_address ) === true ){
          //... given address
          $txid = $mbc->sendtoaddress( $given_address, $amount, 'Cash out from user account '.$user->uid.' to user wallet address '.$this->user_wallet_for_registered_user, 'Thank you for playing' );
          //keep it as new user wallet
          $user->user_wallet_for_registered_user = $given_address;
        }
        else{
          return array( "amount" => -666, "txid" => -666, "details" => 'given_address = '.$given_address );
        }
      }
      //to registered addresses
      else{
        if ( MyBitcoinClient::is_valid_bitcoin_address( $user->user_wallet_for_registered_user ) === true ){
          $txid = $mbc->sendtoaddress( $user->user_wallet_for_registered_user, $amount, 'Cash out from user account '.$user->uid.' to user wallet address '.$user->user_wallet, 'Thank you for playing' );
        }
        elseif ( MyBitcoinClient::is_valid_bitcoin_address( $user->user_wallet ) === true ){
          //... auto detected address
          $txid = $mbc->sendtoaddress( $user->user_wallet, $amount, 'Cash out from user account '.$user->uid.' to user wallet address '.$user->user_wallet, 'Thank you for playing' );
        }
        else{
          return array( "amount" => -666, "txid" => -666, "details" => 'user->user_wallet = '.$user->user_wallet );
        }
      }

      //get amount of fee for withdrawn transaction
      $arr = $mbc->gettransaction( $txid );//$m->query("gettransaction", $txid);
      
      //fee is negative!
      $fee = $arr['fee'];
      //for 'Out' payments we use user balance from DB
      $t = new Transaction( $txid, $amount/*$user->money_balance*/, 0/*deposit == 0 => money was withdraw/outputed */, $user->uid );

      //keep amount of money was moved for returning
      $amount_of_money_was_moved = $amount;//$user->money_balance;
      $user->money_balance -= $amount;
    }

    //----------------------------------------/OUT----------------------------------------------------------------------
    //and SAVE user wallet in DB!
    $user->save_in_db( );
    //money was outputed
    return array( "amount" => $amount_of_money_was_moved, "txid" => $txid );
  }

  public function after_cash_out(){
    //when balance was positive and becomes 0
    if ( 0 == $this->money_balance ){
      $slot = $_SESSION['Slot'];
      //it's time to calculate affiliate
      //                         user A                          user B
      Slot::affiliate_calculate( $this->affiliateusername, $this->uid, $slot->percent_pay_to_referrers);
    }
  }

  //aliases
  public function cash_out( $amount, $cashout_to_given_address, $given_address ){
    try{
      $res_arr = $this->cash_move( $where = 'out', $amount, $cashout_to_given_address, $given_address );
      $this->after_cash_out();
      return $res_arr;
    }
    catch ( Exception $e ) {
      $error_message = "Failed cash out. File ".__FILE__." Line " .__LINE__;
      $error_message .= $e->getTraceAsString();
      error_log( $error_message, 0 );
    }
  }

  public function cash_in(){
    try{
      return $this->cash_move( $where = 'in' );
    }
    catch ( Exception $e ) {
      $error_message = "Failed cash in. File ".__FILE__." Line " .__LINE__;
      $error_message .= $e->getTraceAsString();
      error_log( $error_message, 0 );
    }
  }

  //return array [payed, count]
  public function have_earned_by_affilates(){
    $db = DBconfig::get_instance();
    $query = "SELECT SUM(`payed`) payed FROM `affiliate_link` WHERE `affiliateusername` = '$this->username'";
    $res = $db->query( $query );
    if (!$res){
      $error_message = "Failed in ".__METHOD__.". File ".__FILE__." Line " .__LINE__;
      error_log( $error_message, 0 );
      return FALSE;
    }
    $arr_payed = $db->mysqli_fetch_array_by_result($res);
    if ( !$arr_payed['payed'] ){
      $arr_payed['payed'] = 0;
    }
    return round( $arr_payed['payed'], 8 );
  }
  public function count_by_affilates(){
    $db = DBconfig::get_instance();
    $query = "SELECT COUNT(`uid`) count FROM `users` WHERE `affiliateusername` = '$this->username'";
    $res = $db->query( $query );
    if (!$res){
      $error_message = "Failed in ".__METHOD__.". File ".__FILE__." Line " .__LINE__;
      error_log( $error_message, 0 );
      return FALSE;
    }
    $arr_payed = $db->mysqli_fetch_array_by_result($res);
    $count = $arr_payed['count'];
    return $count;
  }
}