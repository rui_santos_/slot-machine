<?php

/**
 * Description of Payline
 *
 * @author vadim24816
 */
require_once 'Appconfig.php';

//array of 3 symbols like: ['pyramid', 'bitcoin', 'anonymous']
class Payline {
  public $sym1, $sym2, $sym3;
  public $multiplier, $bet_from_client;

  //make the object with given symbols
  public function __construct( $sym1, $sym2, $sym3, $multiplier = 0, $bet_from_client = 0 ) {
    if ( !Symbol::is_symbol( $sym1 ) || !Symbol::is_symbol( $sym2 ) || !Symbol::is_symbol( $sym3 ) ){
      $error_message = "[Warning] Not a symbols was given. Class ".__CLASS__." Method ".__METHOD__." File ".__FILE__." Line " .__LINE__;
      error_log( $error_message, 0 );
      return;
    }
    $this->sym1 = $sym1;
    $this->sym2 = $sym2;
    $this->sym3 = $sym3;
    
    $this->bet_from_client = $bet_from_client;
    $this->$multiplier = $multiplier;
  }
  public function __destruct(){}
  //return symbols as array
  public function get_symbols_array(){
    $syms = array(3);
    $syms[0] = $this->sym1;
    $syms[1] = $this->sym2;
    $syms[2] = $this->sym3;
    return $syms;
  }
}