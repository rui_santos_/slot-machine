<?php
  require_once 'Appconfig.php';
?>
<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8" />
    <title>Slot admin page</title>
    <link rel="stylesheet" href="css/jquery-ui.css" />
    <script src="js/jquery.min.js"></script>
    <script src="bootstrap/js/bootstrap.min.js"></script>
    <script src="js/jquery-ui.js"></script>
  </head>
  <body>
<?php
//admin auth
  function auth(){
    if ( empty($_SESSION['admin']) || $_SESSION['admin'] != 'true' ){
      if ( empty( $_POST['login'] ) || empty( $_POST['password'] ) ){
        exit('Not authorized');
      }
      $login = $_POST['login'];
      $password_md5 = md5($_POST['password']);
      $db = DBconfig::get_instance();
      $admin = $db->mysqli_fetch_array('SELECT * FROM admin');
      //auth == false
      if ( ( $admin['login'] != $login ) || ( $admin['password'] != $password_md5 ) ){
        exit('Wrong login or password');
        return false;
      }
      //auth == true
      $_SESSION['admin'] = true;
      //refresh this page
      echo '<META HTTP-EQUIV="Refresh" Content="0; ">';
    }
    else{
      update_login_pass_in_db();
    }
  }
  //for changing login and pass in db
  function update_login_pass_in_db(){
    if ( empty( $_POST['login'] ) || empty( $_POST['password'] ) || empty( $_POST['Save'] ) ){
      return false;
    }
    if ( empty( $_SESSION['admin'] ) || $_SESSION['admin'] != 'true' || empty( $_POST['Save'] ) ){
      echo 'Can\'t change login and password.';
      return false;
    }
    $login = mysql_real_escape_string( $_POST['login'] );
    $password_md5 = md5( mysql_real_escape_string( $_POST['password'] ) );
    $db = DBconfig::get_instance();
    $login = $db->mysqli_link->real_escape_string( $login );
    $password_md5 = $db->mysqli_link->real_escape_string( $password_md5 );

    $res = $db->query( "UPDATE admin SET login = '$login', password = '$password_md5' WHERE ID = 1" );
    if ( $res ){
      echo 'Login and password updated succesfully<br />';
      echo 'Login: '.$login.'<br>';
      echo 'md5(password): '.$password_md5;
    }
  }
?>
    <div id="auth_form" style="    width: 200px;">
      <form method="POST" action="admin.php" onSubmit="window.location.reload()">
        Login:<br />
        <input type="text" id="login" name="login" />
        <br />
        Password:<br />
        <input type="password" id="password" name="password" />
        <br />
        <?php
          if ( empty($_SESSION['admin'] ) || $_SESSION['admin'] != 'true' ){
        ?>
        <input type="submit" id="enter" name="enter" value="Sign in" />
        <?php
          }
          else{
        ?>
        <input type="submit" id="Save" name="Save" value="Save" />
        <br /><br />
        <?php
          }
        ?>
      </form>
    </div>
<?php
    auth();
?>
    <div id="slot_power_state">Playing status: on</div>
      Slot on: <input id="power" checked="checked" type="checkbox" name="power" />
    <div id="slot_paying_out_state">Paying out status: on</div>
      Paying out on: <input id="paying_out" checked="checked" type="checkbox" name="paying_out" />
    <!--<div id="slot_max_bet">Maximum bet: </div>-->
    <!--  <br />-->
    <!--    Minimum bet: <input id="slot_min_bet" type="text" name="slot_min_bet" />-->
    <!--  <br />-->
    <br />
      Maximum bet: <input id="slot_max_bet" type="text" name="slot_max_bet" />
    <br />
      Percent pay to the referrers: <input id="percent_pay_to_referrers" type="text" name="percent_pay_to_referrers" />
    <br />
    <button id="save_slot_state">Save</button>
    <br />
    <br />
    <div id="calendar">
      <label for="from">From (e.g.: 2012-11-31)</label>
      <input type="text" id="from" name="from" />
      <label for="to">to</label>
      <input type="text" id="to" name="to" />
      <!--<br />-->
      <button id="show_transactions">Show</button>
    </div>
    <div id="transactions">
      <!--      tables are loaded via ajax and placed in this div      -->
    </div>
    <div id="group_by_user">
    </div>
    <script type="text/javascript">
    function slotOptions(){
      this.options = {
        'playing': 'on',
        'paying_out': 'on',
        'maxbet' : 0.01,
        'percent_pay_to_referrers' : 0.3
      };
    }
    $( document ).ready( function(){
      show();
      slotOptions = new slotOptions();
      $(function() {
          $( "#from" ).datepicker({
            dateFormat: 'yy-mm-dd',
            defaultDate: "+1w",
            changeMonth: true,
            numberOfMonths: 1,
            onClose: function( selectedDate ) {
                $( "#to" ).datepicker( "option", "minDate", selectedDate );
            }
          });
          $( "#to" ).datepicker({
            dateFormat: 'yy-mm-dd',
            defaultDate: "+1w",
            changeMonth: true,
            numberOfMonths: 1,
            onClose: function( selectedDate ) {
                $( "#from" ).datepicker( "option", "maxDate", selectedDate );
            }
          });
      });
      getOptionsFromServer();

      $( 'button#save_slot_state' ).on( 'click',function(){
        options = { "is_power_on" : 'on', "is_payingout" : 'on', "maxbet" : 0.01, "minbet" : 0.001, "percent_pay_to_referrers" : 0.3 }
        options.is_power_on = $('input#power').prop('checked') ? 'on' : 'off' ;
        options.is_paying_out = $('input#paying_out').prop('checked') ? 'on' : 'off' ;
        options.minbet = $('input#slot_min_bet').val();
        options.maxbet = $('input#slot_max_bet').val();
        options.percent_pay_to_referrers = $('input#percent_pay_to_referrers').val();
        //save the values by click
        $.post("AjaxRequestsProcessing.php", {
          slot: "options",
          action: "set_options",
          power: options.is_power_on,
          paying_out: options.is_paying_out,
          minbet: options.minbet,
          maxbet: options.maxbet,
          percent_pay_to_referrers: options.percent_pay_to_referrers
        })
          .done(function(options) {
            if (window.console) console.log('options in .done');
            if (window.console) console.log(options);
            options = $.parseJSON(options);
            getOptionsFromServer();
          })
          .fail(function(){
            if (window.console) console.log('Client error. Error in ajax admin-->power request, bad response');
          });
      });
      $('button#show_transactions').on('click',function(){
        show();
      });
      //sync with server
      function getOptionsFromServer(){
        $.post("AjaxRequestsProcessing.php", { slot: "options", power: 'check_options'})
        .done(function(options) {
          options = $.parseJSON(options);
          slotOptions.options.playing = options.playing;
          slotOptions.options.paying_out = options.paying_out;
          slotOptions.options.maxbet = options.maxbet;
          slotOptions.options.minbet = options.minbet;
          slotOptions.options.percent_pay_to_referrers = options.percent_pay_to_referrers;
          fillOptionsOnPage(slotOptions.options);
        })
        .fail(function(){
          if (window.console) console.log('Client error. Error in ajax admin-->checkSlotPowerStatus request, bad response');
        });
      };
      function fillOptionsOnPage(options){
        if (window.console) console.log(options);
        $('div#slot_power_state').text('Playing status: '+options.playing);
        $('div#slot_paying_out_state').text('Paying out status: '+options.paying_out);
        $('input#slot_max_bet').val(options.maxbet);
        $('input#slot_min_bet').val(options.minbet);
        $('input#percent_pay_to_referrers').val(options.percent_pay_to_referrers);
        if (options.playing == 'on'){
          $('input#power').attr('checked', 'checked');
        }
        else{
          $('input#power').removeAttr('checked', 'checked');
        }
        if (options.paying_out == 'on'){
          $('input#paying_out').attr('checked', 'checked');
        }
        else{
          $('input#paying_out').removeAttr('checked', 'checked');
        }
      }

      function show(){
        var fromDate = $('div#calendar > input#from').val();
        var toDate = $('div#calendar > input#to').val();
        if (!fromDate || !toDate){
          if (window.console) console.log('from or to date not specified');
          fromDate = '2012-11-01';
          toDate = '2052-11-01';
        }
        $.post("AjaxRequestsProcessing.php", { slot: "transactions", 'fromDate': fromDate, 'toDate': toDate, 'page':'admin'})
          .done(function(transactionsTable) {
            $('div#transactions').html(transactionsTable);
          })
          .fail(function(){
            if (window.console) console.log('Client error. Error in ajax admin-->show request, bad response');
          });
      }
    });
  </script>
  </body>
</html>
