<?php
/**
 * Created by JetBrains PhpStorm.
 * User: vadim24816
 * Date: 09.04.13
 * Time: 23:20
 * To change this template use File | Settings | File Templates.
 */

require_once 'Appconfig.php';
require_once 'Header.php';
$user = $_SESSION['User'];
?>
<body>
  <div id="affiliates">
    <table class="table" id="">
      <tr>
        <td>
          Your affiliate link
        </td>
        <td>
          Have earned
        </td>
        <td>
          How many people have referred
        </td>
      </tr>
      <tr>
        <td>
          <a href="
          <?php
          //if registered - show aff link
          if ( $user->is_registered === true ){
            echo $affiliate_link_url = "https://" . $_SERVER['HTTP_HOST'] . "/?r=" . $user->username;
          }
          ?>" id="affiliate_link"><?php echo $affiliate_link_url; ?></a>
        </td>
        <td>
          <?php
            echo $user->have_earned_by_affilates();
          ?>
        </td>
        <td>
          <?php
            echo $user->count_by_affilates();
          ?>
        </td>
      </tr>
    </table>
  </div>
</body>
</html>