<?php
/**
 * Description of AjaxRequestsProcessing
 *
 * @author vadim24816
 */

//for special requests
define("SPECIAL_REQUEST_LIMIT_TIME_INTERVAL", 0.01);
//for all requests to AjaxRequestsProcessing.php
define("GLOBAL_REQUEST_LIMIT_TIME_INTERVAL", 0.001);

$time_start = microtime(true);
require_once 'Appconfig.php';

//set limit for request amount from the same user
//true - no more requests!
//false - save last_request_time and let make requests
function is_limit_time_interval_expired( $request_limit_time_interval ) {
  //$request_limit_time_interval = 0.2;//1 request per $request_limit_time_interval sec
  //if not set, set to current time
  if ( !isset($_SESSION['last_request_time']) ){
    //session_start();
    $_SESSION['last_request_time'] = microtime( true );
    //session_write_close();
  }
  //if request_limit_time_interval is expired
  if ( (microtime( true ) - $_SESSION['last_request_time'] )  > $request_limit_time_interval ){
    //save current time
    $_SESSION['last_request_time'] = microtime( true );
    return false;
  }
  else{
    //output error in response
    echo '{"request_processing" : "false",
           "time_since_last_request" : "'. ( microtime( true ) - $_SESSION['last_request_time'] ) .'",
           "time_limit" : "'. $request_limit_time_interval .'"  }';
    return true;
  }
}

if (!empty($_POST['slot'])){
  $post_request = $_POST['slot'];
}
else {
  //redirect to main page
  header('Location: https://'.$_SERVER['HTTP_HOST']);
  $error_message = "[Warning] Parameter 'slot' not given in POST array.  File ".__FILE__." Line " .__LINE__;
  error_log( $error_message, 0 );
  exit('No slot option in POST array');
}


$user = (empty($_SESSION['User']))? User::get_instance() : $_SESSION['User'];
$slot = (empty($_SESSION['Slot']))? Slot::get_instance($_SESSION['User']) : $_SESSION['Slot'];

switch ($post_request) {
  case 'signin':
    $username = $_POST['username'];
    $password = $_POST['password'];
    $manual_login_result = $user->manual_login( $username, $password );
    //logged in
    if ( true === $manual_login_result ){
      $response = json_encode( array( 'signin_result' => true , 'error_message' => '' ) );
    }
    //not logged in
    elseif ( $manual_login_result === -1 ){
      $response = json_encode( array( 'signin_result' => false , 'error_message' => 'Username doesn\'t exist. Register user, please' ) );
    }
    else {
      $response = json_encode( array( 'signin_result' => false , 'error_message' => 'Wrong login or password', 'return_code' => $manual_login_result ) );
    }
    echo $response;
    break;

  case 'signout':
    $manual_logout_result = $user->manual_logout();
    if ( $manual_logout_result === true ){
      $response = json_encode( array( 'signout_result' => true , 'error_message' => '' ) );
    }
    else{
      $response = json_encode( array( 'signout_result' => false , 'error_message' => 'Unknown logout error' ) );
    }
    echo $response;
    break;

  //try to register user
  case 'signup':
    $username = $_POST['username'];
    $password = $_POST['password'];
    $confirm_password = $_POST['confirm_password'];
    //check all given params
    $manual_reg_result = $user->manual_reg( $username, $password, $confirm_password );
    if ( true === $manual_reg_result ){
      //login right after registration
      $manual_login_result = $user->manual_login( $username, $password );
      $response = json_encode( array( 'signup_result' => true , 'signin_result' => $manual_login_result, 'error_message' => '' ) );
    }
    elseif ( -1 === $manual_reg_result ){
      $response = json_encode( array( 'signup_result' => false , 'error_message' => 'Username already exists, enter another username, please' ) );
    }
    elseif ( -2 === $manual_reg_result ){
      $response = json_encode( array( 'signup_result' => false , 'error_message' => 'Username or password/confirm_password not valid' ) );
    }
    echo $response;
    break;

  case 'cashOut':
    try{
      $amount = 0;
      if (!empty($_POST['amount'])){
        $amount = $_POST['amount'];
      }
      //get address entered by user
      $user_entered_wallet = $_POST['user_entered_wallet'];
      if ( MyBitcoinClient::is_valid_bitcoin_address( $user_entered_wallet ) ){
        $user->user_wallet_for_registered_user = $user_entered_wallet;
        $cashout_to_given_address = true;
      }
      else{
        return array("amount" => -666, "txid" => -666, "details" => "Bitcoin address is not valid");
      }
      $out =  $user->cash_out( $amount, $cashout_to_given_address, $user_entered_wallet );
      $out = json_encode( $out );
      if ( $out ){
        echo $out;
      }
      else{
        echo '0';
      }
    }
    catch (Exception $e){
      $error_message = "Cash out failed. File ".__FILE__." Line " .__LINE__;
      $error_message .= $e->getTraceAsString();
      error_log( $error_message, 0 );
    }
    break;
  case 'getHashedServerSeeds':
    $server_seeds = $slot->getHashedServerSeeds();
    $json = json_encode( $server_seeds );
    echo $json;
    break;
  case 'sync':
    $now_time = time();
    //check matching for $_SESSION['window_name'] and window.name
    if ($_POST['action'] == 'check_window_name'){
      //todo: make for 4 opened windows
      if ( isset($_SESSION['window_name']) ){
        $opened_window_data = array( 'isset' => 'yes', 'window_name' => $_SESSION['window_name'] );
      }
      else{
        $_SESSION['window_name'] = md5( $now_time.mt_rand( 0,1000 ) );
        $opened_window_data = array( 'isset' => 'no', 'window_name' => $_SESSION['window_name'] );
      }
    }
    //set new value for $_SESSION['window_name']
    else if ($_POST['action'] == 'set_new_window_name'){
      $_SESSION['window_name'] = md5($now_time.mt_rand(0,1000));
      $opened_window_data = array('window_name' => $_SESSION['window_name']);
    }
    //check and set register state
    if ( $user->is_registered() ){
      $user->is_registered = true;
    }
    $user->update_from_db();
    $return_string = json_encode( array( 'user' => $user, 'opened_window' => $opened_window_data ) );
    if ( $return_string ){
      echo $return_string;
    }
    break;

  case 'spinPressed':
    if ( is_limit_time_interval_expired(0) ){
      return false;
    }
    //no bet
    if ( !isset( $_POST['currentBet'] ) || !isset( $_POST['clientSeed'] ) ){
      $error_message = "[Warning] Bet wasn't transferred or client seed is wrong. File ".__FILE__." Line " .__LINE__;
      error_log( $error_message, 0 );
      return false;
    }
    //normal mode
    $client_seed = $_POST['clientSeed'];
    $betFromClient = $_POST['currentBet'];
    try{
      $arr_of_spin_res = $slot->spin( $betFromClient, $client_seed );
    }
    catch ( Exception $e ){
      $error_message = "spinPressed failed. File ".__FILE__." Line " .__LINE__;
      $error_message .= $e->getTraceAsString();
      error_log( $error_message, 0 );
    }
    //check for errors
    if ( !is_array($arr_of_spin_res) ){
      if ( is_numeric( $arr_of_spin_res ) )
      //echo '-3';
      echo (string)$arr_of_spin_res;
      //echo 'Wrong spin';
      return false;
    }
    $time_end = microtime(true);
    $script_time = $time_end-$time_start;
    $script_time = sprintf('%.4F s', $script_time);
    $arr_of_spin_res_with_script_time = array_merge($arr_of_spin_res, array('spin_script_time' => $script_time));
    $json = json_encode($arr_of_spin_res_with_script_time);
    echo $json;
    break;

  case 'checkForIncommingPayment':
    if ( is_limit_time_interval_expired(SPECIAL_REQUEST_LIMIT_TIME_INTERVAL) ){
      return false;
    }
    $cashed_in_value = $user->cash_in();
    $cashed_in_value = json_encode($cashed_in_value);
    $time_end = microtime(true) - $time_start;
    echo $cashed_in_value;//.' '.$time_end;
    break;
    
  case 'getInterestingFacts':
    $cashed_out_money = Transaction::get_total_cashed_out_money();
    $total_spin_number = $slot->get_total_spin_number();
    $json = '{"cashed_out_money":"'.$cashed_out_money.'","games_played":"'.$total_spin_number.'"}';
    echo $json;
    break;
  case 'options':
    if ( empty( $_POST['power'] ) ){
      return false;
    }
    if ($_POST['power'] == 'check_options'){
      $options['playing'] = $slot->get_option( 'playing' );
      $options['paying_out'] = $slot->get_option( 'paying_out' );
      $options['maxbet'] = $slot->get_option( 'maxbet' );
      $options['minbet'] = $slot->get_option( 'minbet' );
      $options['percent_pay_to_referrers'] = $slot->get_option( 'percent_pay_to_referrers' );
      echo json_encode( $options );
      return true;
    }
    //for admin only!
    if ( $_SESSION['admin'] && $_POST['action'] == 'set_options' ){
      $received_options = array(
        'playing' => $_POST['power'],
        'paying_out' => $_POST['paying_out'],
        'maxbet' => $_POST['maxbet'],
        'minbet' => $_POST['minbet'],
        'percent_pay_to_referrers' => $_POST['percent_pay_to_referrers']
      );
      $setted_options = $slot->set_options( $received_options );
        echo json_encode( $setted_options );
    }
    break;
    
    case 'transactions':
      $table = 'Wrong params';
      //between from and to dates
      if ( !empty( $_POST['fromDate'] ) && !empty( $_POST['toDate'] ) ){
        $fromDate = ( $_POST['fromDate'] );
        $toDate = ( $_POST['toDate'] );
        //if request from admin page
        if ( !empty( $_POST['page'] ) && $_POST['page'] == 'admin' && $_SESSION['admin'] ){
          $table = Transaction::show_transactions( 'transactions',0, 0, $fromDate, $toDate, 'admin' );
          $table .= Transaction::show_grouped_by_user( $fromDate, $toDate );
          $table .= Transaction::show_cash_in_out_profit_payback_table( $fromDate, $toDate );
        }
        //if request from other page
        else{
          $table = Transaction::show_transactions( 'transactions',0, 0, $fromDate, $toDate );
        }
      }
      if ( !empty( $_POST['to'] ) && !empty( $_POST['option'] ) ){
        $option = $_POST['option'];
        $from = $_POST['from'];
        $to = $_POST['to'];
        $table = Transaction::show_transactions( $option, $from, $to );
      }
      echo $table;
      break;
    case 'bitcoin_connect':
      if ( is_limit_time_interval_expired( SPECIAL_REQUEST_LIMIT_TIME_INTERVAL ) ){
        return false;
      }
      $mbc = (empty($_SESSION['MyBitcoinClient']))? MyBitcoinClient::get_instance() : $_SESSION['MyBitcoinClient'];
      echo $mbc->check_last_connect();
      break;

  default:
    echo '{"error" : "Wrong request" }';
    $error_message = "[Warning] Wrong request. File ".__FILE__." Line " .__LINE__;
    error_log( $error_message, 0 );
    break;
}